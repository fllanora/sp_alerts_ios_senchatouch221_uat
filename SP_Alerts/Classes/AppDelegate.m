/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */

//
//  AppDelegate.m
//  SP_Alerts
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright ___ORGANIZATIONNAME___ ___YEAR___. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"

#import <Cordova/CDVPlugin.h>

#import "PushPlugin.h"
#import "KeychainPlugin.h"

@implementation AppDelegate

@synthesize invokeString, window, viewController, webView;
NSInteger alertCount;
@synthesize message_alert, message_alertid, userInfo;


- (id)init
{
    /** If you need to do any extra app-specific initialization, you can do it here
     *  -jm
     **/
    NSHTTPCookieStorage* cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];

    [cookieStorage setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];

    int cacheSizeMemory = 8 * 1024 * 1024; // 8MB
    int cacheSizeDisk = 32 * 1024 * 1024; // 32MB
#if __has_feature(objc_arc)
        NSURLCache* sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"nsurlcache"];
#else
        NSURLCache* sharedCache = [[[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"nsurlcache"] autorelease];
#endif
    [NSURLCache setSharedURLCache:sharedCache];

    self = [super init];
    return self;
}

#pragma mark UIApplicationDelegate implementation

/**
 * This is main kick off after the app inits, the views and Settings are setup here. (preferred - iOS4 and up)
 */
- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    
    //Clear keychain on first run in case of reinstallation
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"spkeychain"]) {
        // Delete values from keychain here
        
        NSString* key = @"userid";
        NSString* serviceName =  @"spkeychain";
        NSError* error = nil;
        
        BOOL deleted = [SFHFKeychainUtils deleteItemForUsername:key andServiceName:serviceName error:&error];
        
        key = @"password";
        serviceName =  @"spkeychain";
        
        deleted = [SFHFKeychainUtils deleteItemForUsername:key andServiceName:serviceName error:&error];
        
        key = @"cookie";
        serviceName =  @"spkeychain";
        
        deleted = [SFHFKeychainUtils deleteItemForUsername:key andServiceName:serviceName error:&error];
        
        
        [[NSUserDefaults standardUserDefaults] setValue:@"1strun" forKey:@"spkeychain"];
    }
    
    
    userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
    if (userInfo) {
        
        NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];
        NSString *alertMsg = @"";
        NSString *badge = @"";
        NSString *sound = @"";
        NSString *custom = @"";
        
        self.message_alertid = [userInfo valueForKey:@"id"];
        NSLog(@"message id %@",self.message_alertid);
        
        
        
        
        if( [apsInfo objectForKey:@"alert"] != NULL)
        {
            alertMsg = [apsInfo objectForKey:@"alert"];
            NSLog(@"alert %@",alertMsg);
            NSLog(@"message %@",alertMsg);
            self.message_alert = alertMsg;
            NSInteger bageNumber;
        }
        
        if( [apsInfo objectForKey:@"badge"] != NULL)
        {
            badge = [apsInfo objectForKey:@"badge"];
            NSLog(@"badge %@",badge);
        }
        
        
        if( [apsInfo objectForKey:@"sound"] != NULL)
        {
            sound = [apsInfo objectForKey:@"sound"];
            NSLog(@"sound %@",sound);
        }
        
        if( [userInfo objectForKey:@"Custom"] != NULL)
        {
            custom = [userInfo objectForKey:@"Custom"];
            NSLog(@"Custom %@",custom);
        }
        
        
    }else {
        NSLog(@">>>>> NOTFICATION ERROR - ");
        
    }
    
    
    
    
    NSURL* url = [launchOptions objectForKey:UIApplicationLaunchOptionsURLKey];
    if (url && [url isKindOfClass:[NSURL class]]) {
        self.invokeString = [url absoluteString];
		NSLog(@"SP_ALERT launchOptions = %@", url);
    }    

    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];

#if __has_feature(objc_arc)
        self.window = [[UIWindow alloc] initWithFrame:screenBounds];
#else
        self.window = [[[UIWindow alloc] initWithFrame:screenBounds] autorelease];
#endif
    self.window.autoresizesSubviews = YES;

#if __has_feature(objc_arc)
        self.viewController = [[MainViewController alloc] init];
#else
        self.viewController = [[[MainViewController alloc] init] autorelease];
#endif
    self.viewController.useSplashScreen = YES;

    // Set your app's start page by setting the <content src='foo.html' /> tag in config.xml.
    // If necessary, uncomment the line below to override it.
    // self.viewController.startPage = @"index.html";

    // NOTE: To customize the view's frame size (which defaults to full screen), override
    // [self.viewController viewWillAppear:] in your view controller.
    self.viewController.webView.delegate = self;
    
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];

    return YES;
}

- (void)applicationDidFinishLaunching:(UIApplication *)app {
    
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
    NSLog(@"applicationDidFinishLaunching");
    
}

// this happens while we are running ( in the background, or from within our own app )
// only valid if SP_Alerts-Info.plist specifies a protocol to handle
- (BOOL)application:(UIApplication*)application handleOpenURL:(NSURL*)url
{
    if (!url) {
        return NO;
    }

    // calls into javascript global function 'handleOpenURL'
    NSString* jsString = [NSString stringWithFormat:@"handleOpenURL(\"%@\");", url];
    [self.viewController.webView stringByEvaluatingJavaScriptFromString:jsString];

    // all plugins will get the notification, and their handlers will be called
    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:CDVPluginHandleOpenURLNotification object:url]];

    return YES;
}

// repost the localnotification using the default NSNotificationCenter so multiple plugins may respond
- (void)            application:(UIApplication*)application
    didReceiveLocalNotification:(UILocalNotification*)notification
{
    // re-post ( broadcast )
    [[NSNotificationCenter defaultCenter] postNotificationName:CDVLocalNotification object:notification];
}


#pragma PGCommandDelegate implementation

- (id) getCommandInstance:(NSString*)className
{
	return [self.viewController getCommandInstance:className];
}


- (NSString*) pathForResource:(NSString*)resourcepath;
{
	return [self.viewController pathForResource:resourcepath];
}

#pragma UIWebDelegate implementation

- (void) webViewDidFinishLoad:(UIWebView*) theWebView
{
	// only valid if FooBar.plist specifies a protocol to handle
	if (self.invokeString)
	{
		// this is passed before the deviceready event is fired, so you can access it in js when you receive deviceready
		NSString* jsString = [NSString stringWithFormat:@"var invokeString = \"%@\";", self.invokeString];
		[theWebView stringByEvaluatingJavaScriptFromString:jsString];
	}
	
    // Black base color for background matches the native apps
   	theWebView.backgroundColor = [UIColor blackColor];
    
	return [self.viewController webViewDidFinishLoad:theWebView];
}

- (void) webViewDidStartLoad:(UIWebView*)theWebView
{
	return [self.viewController webViewDidStartLoad:theWebView];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    PushPlugin *pushHandler = [self getCommandInstance:@"PushPlugin"];
    [pushHandler didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
    
    PushPlugin *pushMessageIDHandler = [self getCommandInstance:@"PushPlugin"];
    [pushMessageIDHandler setMessageID:self.message_alertid];
    PushPlugin *pushHandlerMessage = [self getCommandInstance:@"PushPlugin"];
    [pushHandlerMessage setMessageAlert:self.message_alert];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive) {
    } else {
        NSLog(@">>>>>>> Do stuff that you would do if the application was not active");
        if (userInfo) {
            NSString *messageAlertID =  [userInfo valueForKey:@"id"];
            self.message_alertid = [userInfo valueForKey:@"id"];
            NSLog(@"message id %@",self.message_alertid);
            PushPlugin *pushHandler = [self getCommandInstance:@"PushPlugin"];
            [pushHandler setMessageID:self.message_alertid];
        }
        NSLog(@">>>>>>> Active Event (NATIVE)");
        [self.webView stringByEvaluatingJavaScriptFromString:@"cordova.fireDocumentEvent('active');"];
        
        
    }
}



- (void) webView:(UIWebView*)theWebView didFailLoadWithError:(NSError*)error
{
	return [self.viewController webView:theWebView didFailLoadWithError:error];
}

- (BOOL) webView:(UIWebView*)theWebView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType
{
	return [self.viewController webView:theWebView shouldStartLoadWithRequest:request navigationType:navigationType];
}



- (NSUInteger)application:(UIApplication*)application supportedInterfaceOrientationsForWindow:(UIWindow*)window
{
    // iPhone doesn't support upside down by default, while the iPad does.  Override to allow all orientations always, and let the root view controller decide what's allowed (the supported orientations mask gets intersected).
    NSUInteger supportedInterfaceOrientations = (1 << UIInterfaceOrientationPortrait) | (1 << UIInterfaceOrientationLandscapeLeft) | (1 << UIInterfaceOrientationLandscapeRight) | (1 << UIInterfaceOrientationPortraitUpsideDown);

    return supportedInterfaceOrientations;
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication*)application
{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

@end

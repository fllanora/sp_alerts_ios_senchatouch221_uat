

#import "OfflineCache.h"
#import <Cordova/CDV.h>


@implementation OfflineCache


/*****************************************************
 * 
 * Webview Initialization
 * 
 *****************************************************/
-(CDVPlugin*) initWithWebView:(UIWebView*)theWebView
{
    self = (OfflineCache*)[super initWithWebView:theWebView];
    return self;
}


/*****************************************************
 * 
 * Entry point to  the javascript plugin for PhoneGap
 * 
 *****************************************************/
- (void)downloadFile:(CDVInvokedUrlCommand*)command
{
//-(void) downloadFile:(NSMutableArray*)paramArray withDict:(NSMutableDictionary*)options {
	//NSString* userID = [command.arguments objectAtIndex:1];
	NSLog(@"in OfflineCache.downloadFile",nil);
	NSString * sourceUrl = [command.arguments objectAtIndex:0];
	NSString * fileName = [command.arguments objectAtIndex:1];
	//NSString * completionCallback = [paramArray objectAtIndex:2];
	
	params = [[NSMutableArray alloc] initWithCapacity:2];	
	[params addObject:sourceUrl];
	[params addObject:fileName];
	
	[self downloadFileFromUrl:params];
}

/*****************************************************
 * 
 * Call to excute the download in a background thread
 * 
 *****************************************************/
-(void) downloadFileFromUrl:(NSMutableArray*)paramArray
{
	NSLog(@"in OfflineCache.downloadFileFromUrl",nil);
	[self performSelectorInBackground:@selector(downloadFileFromUrlInBackgroundTask:) withObject:paramArray];
}

/*****************************************************
 * 
 * Downloads the file in the background and saves it to the local documents
 * directory for the application
 * 
 *****************************************************/
-(void) downloadFileFromUrlInBackgroundTask:(NSMutableArray*)paramArray
{
	NSLog(@"in OfflineCache.downloadFileFromUrlInBackgroundTask",nil);
	NSString * sourceUrl = [paramArray objectAtIndex:0];
	NSString * fileName = [paramArray objectAtIndex:1];
	
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSData* theData = [NSData dataWithContentsOfURL: [NSURL URLWithString:sourceUrl] ];
	
    /*****************************************************
     *  save file in documents directory
     *****************************************************/
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];	
	
	NSString *newFilePath = [documentsDirectory stringByAppendingString:[NSString stringWithFormat: @"/%@", fileName]];
    //NSString *newFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingString:fileName];
	
	NSLog(@"Writing file to path %@", newFilePath);
	//NSFileManager *fileManager=[NSFileManager defaultManager];
	NSError *error=[[[NSError alloc]init] autorelease]; 
	
	BOOL response = [theData writeToFile:newFilePath options:NSDataWritingFileProtectionNone error:&error];
	
	if ( response == NO ) {
	//	NSLog(@"file save result %@", [error description]);
        
        /*****************************************************
         * send our results back to the main thread 
         *****************************************************/
		[self performSelectorOnMainThread:@selector(downloadCompleteWithError:)  
							   withObject:[error description] waitUntilDone:YES];  	
        
	} else {
		NSLog(@"No Error, file saved successfully", nil);
		
        /*****************************************************
         * send our results back to the main thread 
         *****************************************************/
		[self performSelectorOnMainThread:@selector(downloadComplete:)  
							   withObject:newFilePath waitUntilDone:YES];  	
		
	}
	[pool drain];
}

/*****************************************************
 * 
 * Calls the predefined callback in the ui to indicate completion
 * directory for the application
 * 
 *****************************************************/
-(void) downloadComplete:(NSString *)filePath {
	NSLog(@"in OfflineCache.downloadComplete",nil);	
	NSString * jsCallBack = [NSString stringWithFormat:@"OfflineCacheDownloadComplete('%@');",filePath];    
	[self writeJavascript: jsCallBack];
}

/*****************************************************
 * 
 * Calls the predefined callback in the ui to indicate completion with error
 * 
 *****************************************************/
-(void) OfflineCacheDownload:(NSString *)errorStr {
	NSLog(@"in OfflineCache.downloadCompleteWithError",nil);	
	NSString * jsCallBack = [NSString stringWithFormat:@"OfflineCacheDownloadCompleteWithError('%@');",errorStr];    
	[self writeJavascript: jsCallBack];	
}

- (void)dealloc
{
	if (params) {
		[params release];
	}
    [super dealloc];
}


@end

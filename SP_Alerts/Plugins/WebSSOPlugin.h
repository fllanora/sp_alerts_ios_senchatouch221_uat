/********* Echo.h Cordova Plugin Header *******/

#import <Cordova/CDV.h>

@interface WebSSOPlugin : CDVPlugin {
    /*****************************************************
     * WebSSO Variables
     *****************************************************/
	NSString* callbackID;
	NSString* cookieHeaderAll;
	NSString* cookieHeaderTemp01;
	NSString* cookieHeaderTemp02;
    NSString* cookieStored;
	NSMutableData *receivedData;
	NSString* postType;
    NSString* responsetext;
    NSString* webSSODomainString;
	NSMutableURLRequest *request;
}

@property (nonatomic, copy) NSString* callbackID;
@property (nonatomic, copy) NSString* cookieHeaderTemp01;
@property (nonatomic, copy) NSString* cookieHeaderTemp02;
@property (nonatomic, copy) NSString* cookieHeaderAll;
@property (nonatomic, copy) NSString* cookieStored;
@property (nonatomic, copy) NSString* postType;
@property (nonatomic, copy) NSMutableData *receivedData;
@property (nonatomic, copy) NSString* responsetext;
@property (nonatomic, copy) NSString* webSSODomainString;
@property (nonatomic, copy) NSMutableURLRequest *request;


/*****************************************************
 * Instance Method
 *****************************************************/
- (void)formPost:(CDVInvokedUrlCommand*)command;
- (void)setWebSSODomain:(CDVInvokedUrlCommand*)command;
- (void)httpRequest:(CDVInvokedUrlCommand*)command;

@end


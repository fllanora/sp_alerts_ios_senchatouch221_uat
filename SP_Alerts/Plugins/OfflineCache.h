
#import <Foundation/Foundation.h>
#import <Cordova/CDV.h>

@interface OfflineCache : CDVPlugin {
	NSMutableArray* params;
}

/*****************************************************
 * 
 * Methods
 * 
 *****************************************************/
- (void)downloadFile:(CDVInvokedUrlCommand*)command;
-(void) downloadComplete:(NSString *)filePath;
-(void) downloadCompleteWithError:(NSString *)errorStr; 
-(void) downloadFileFromUrlInBackgroundTask:(NSMutableArray*)paramArray;
-(void) downloadFileFromUrl:(NSMutableArray*)paramArray;
@end

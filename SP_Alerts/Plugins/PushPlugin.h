

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <Cordova/CDV.h>

@interface PushPlugin : CDVPlugin {
    
	NSString* callbackID;
	NSString* cookieHeaderAll; 
    NSString* messageID; 
    NSString* messageAlert; 
/*	NSString* cookieHeaderTemp01;
	NSString* cookieHeaderTemp02;
	NSURLConnection *theConnection;
	NSMutableData *receivedData;
	NSString* postType;
	NSMutableURLRequest *request;
 */
}

@property (nonatomic, copy) NSString* callbackID;
//@property (nonatomic, copy) NSString* cookieHeaderTemp01;
//@property (nonatomic, copy) NSString* cookieHeaderTemp02;
@property (nonatomic, copy) NSString* cookieHeaderAll; 
@property (nonatomic, copy) NSString* messageID; 
@property (nonatomic, copy) NSString* messageAlert; 
//@property (nonatomic, copy) NSString* postType;

//Instance Method
- (void)registerDevice:(CDVInvokedUrlCommand*)command;
- (void)getDeviceToken:(CDVInvokedUrlCommand*)command;
- (void)decreaseNotificationBadgeCount:(CDVInvokedUrlCommand*)command;

- (void)getMessageAlert:(CDVInvokedUrlCommand*)command;
- (void)setNotificationBadgeCount:(CDVInvokedUrlCommand*)command;
- (void)setMessageId:(CDVInvokedUrlCommand*)command;
- (void)getMessageId:(CDVInvokedUrlCommand*)command;
- (void) setMessageAlert:(NSString*)alertmessageAlert;
- (void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;
- (void)didFailToRegisterForRemoteNotificationsWithError:(NSError *)error;
- (void)didRegisterForRemoteNotificationsWithDeviceToken2:(NSData *)deviceToken2;
- (void)didFailToRegisterForRemoteNotificationsWithError2:(NSError *)error2;

@end
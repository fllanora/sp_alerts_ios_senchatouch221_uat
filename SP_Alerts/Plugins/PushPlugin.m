
#import "PushPlugin.h"
#import <Cordova/CDV.h>

@implementation PushPlugin 

@synthesize callbackID;
@synthesize cookieHeaderAll;
@synthesize messageID;
@synthesize messageAlert;


- (void)registerDevice:(CDVInvokedUrlCommand*)command
{
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    self.callbackID = command.callbackId;
}


- (void)decreaseNotificationBadgeCount:(CDVInvokedUrlCommand*)command
{
//-(void)decreaseNotificationBadgeCount:(NSMutableArray*)arguments withDict:(NSMutableDictionary*)options
//{
   // [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
    NSInteger bageNumber;
    bageNumber =  [[UIApplication sharedApplication] applicationIconBadgeNumber] - 1;
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:bageNumber];
    
}


- (void)setNotificationBadgeCount:(CDVInvokedUrlCommand*)command
{
    
//-(void)setNotificationBadgeCount:(NSMutableArray*)arguments withDict:(NSMutableDictionary*)options
//{
	self.callbackID = command.callbackId;
	NSString* badgeNumberString = [command.arguments objectAtIndex:0];
    NSInteger badgeNumber = [badgeNumberString intValue];
    NSLog(@"BADGE NUMBER: %d", badgeNumber);
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badgeNumber];
}






- (void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken2{
    
	NSLog(@"My token is2: %@", deviceToken2);
    NSString *token = [[[[deviceToken2 description] stringByReplacingOccurrencesOfString:@"<"withString:@""] 
                        stringByReplacingOccurrencesOfString:@">" withString:@""] 
                       stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken:%@", token);
    
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:[token stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:self.callbackID];

/*
    PluginResult* pluginResult = [PluginResult resultWithStatus:PGCommandStatus_OK messageAsString:[token stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [self writeJavascript: [pluginResult toSuccessCallbackString:self.callbackID]];
 */
    
  }

- (void)didFailToRegisterForRemoteNotificationsWithError2:(NSError*)error
{
	NSLog(@"Failed to get token, error2: %@", error);
}


- (void)setMessageId:(CDVInvokedUrlCommand*)command
{
//- (void)setMessageId:(NSMutableArray*)arguments withDict:(NSMutableDictionary*)options
//{
 
    self.callbackID = command.callbackId;
    NSString* messageIDset = [command.arguments objectAtIndex:0];
    messageID = messageIDset;
    NSLog(@"setMessageID: %@", messageID);
      // messageID = alertmessageId; 
   // self.callbackID = [arguments pop];
   // PluginResult* pluginResult = [PluginResult resultWithStatus:PGCommandStatus_OK messageAsString:self.messageID];
   // [self writeJavascript: [pluginResult toSuccessCallbackString:self.callbackID]];
    
}


- (void)getMessageId:(CDVInvokedUrlCommand*)command
{
//-(void)getMessageId:(NSMutableArray*)arguments withDict:(NSMutableDictionary*)options
//{
    NSLog(@"getMessageId: %@", self.messageID);
   //  self.callbackID = [arguments pop];
  //  PluginResult* pluginResult = [PluginResult resultWithStatus:PGCommandStatus_OK messageAsString:self.messageID];
   // [self writeJavascript: [pluginResult toSuccessCallbackString:self.callbackID]];
    
     self.callbackID = command.callbackId;
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:self.messageID];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:self.callbackID];
    

}

- (void)setMessageAlert:(NSString *)alertmessageAlert
{
    NSLog(@"setMessageAlert: %@", alertmessageAlert);
    messageAlert = alertmessageAlert; 
}

- (void)getMessageAlert:(CDVInvokedUrlCommand*)command
{
//- (void) getMessageAlert:(NSMutableArray*)arguments withDict:(NSMutableDictionary*)options
//{
  /*
    NSLog(@"getMessageAlert: %@", self.messageAlert);
    self.callbackID = [arguments pop];
    PluginResult* pluginResult = [PluginResult resultWithStatus:PGCommandStatus_OK messageAsString:[self.messageAlert stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [self writeJavascript: [pluginResult toSuccessCallbackString:self.callbackID]];
   */
     self.callbackID = command.callbackId;
    
     NSLog(@"getMessageAlert: %@", self.messageAlert);
   //  self.callbackID = [arguments pop];
    // PluginResult* pluginResult = [PluginResult resultWithStatus:PGCommandStatus_OK messageAsString:self.messageAlert];
   //  [self writeJavascript: [pluginResult toSuccessCallbackString:self.callbackID]];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:self.messageAlert];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:self.callbackID];
    
    
}

@end
/********* Echo.m Cordova Plugin Implementation *******/

#import "WebSSOPlugin.h"
#import <Cordova/CDV.h>

@implementation WebSSOPlugin

@synthesize callbackID;
@synthesize cookieHeaderTemp01;
@synthesize cookieHeaderTemp02;
@synthesize cookieHeaderAll;
@synthesize cookieStored;
@synthesize postType;
@synthesize receivedData;
@synthesize responsetext;
@synthesize webSSODomainString;
@synthesize request;

/*****************************************************
 *
 * Perform WebSSO Form Post Request
 *
 * @param {String} data.getString(1) userID
 * @param {String} data.getString(2) user password
 * @param {String} data.getString(3) cookie session
 * @param {String} data.getString(4) type of login
 *
 *****************************************************/
- (void)formPost:(CDVInvokedUrlCommand*)command
{
    // NSString* echo = [command.arguments objectAtIndex:0];
    self.callbackID = command.callbackId;//[command.arguments pop];
	self.cookieHeaderAll = @"";
	self.cookieHeaderTemp01 = @"";
	self.cookieHeaderTemp02 = @"";
    
	
    /*****************************************************
     * Gets the userID and password
     *****************************************************/
    NSLog(@"LOG: Gets the userID and password");
	NSString* userID = [command.arguments objectAtIndex:1];
	NSString* userPassword = [command.arguments objectAtIndex:2];
    self.cookieStored =  [command.arguments objectAtIndex:3];
    
	NSString *post = [NSString stringWithFormat:@"username=%@&password=%@&login-form-type=pwd", userID, userPassword];
    
    NSLog(@"LOG: post %@",post);
	self.postType = [command.arguments objectAtIndex:4];
    
    NSLog(@"LOG: self.postType %@",self.postType);
    /*****************************************************
     * WebSSO Formpost Authentication using Credentials
     *****************************************************/
	if([postType isEqual: @"credentialsLogin"])
	{
        NSLog(@"LOG: WebSSO Formpost Authentication using Credentials");
		NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
		NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
        /*****************************************************
         * Define Mutable URL Request
         *****************************************************/
        NSMutableURLRequest *postRequest =nil;
        postRequest  = [[NSMutableURLRequest alloc] init];
        [postRequest setHTTPShouldHandleCookies:NO];
        [postRequest setHTTPShouldUsePipelining:NO];
        
        if(self.webSSODomainString == nil)
        {
            self.webSSODomainString = @"sso.sp.edu.sg";
        }
        /*****************************************************
         * Check the WebSSO Environment server type
         *****************************************************/
        NSLog(@"LOG: Check the WebSSO Environment server type");
        NSRange range = [webSSODomainString rangeOfString: @"testsp"];
        if (range.location == NSNotFound){
            /*****************************************************
             * Production server Form Post Url
             *****************************************************/
            [postRequest setURL:[NSURL URLWithString:@"https://sso.sp.edu.sg/pkmslogin.form"]];
        }else {
            /*****************************************************
             * Test server Form Post Url
             *****************************************************/
            NSString* webSSOURL = [NSString stringWithFormat:@"https://%@/pkmslogin.form", webSSODomainString];
            [postRequest setURL:[NSURL URLWithString:webSSOURL]];
        }
        [postRequest setHTTPMethod:@"POST"];
        [postRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [postRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [postRequest setHTTPBody:postData];
        
        NSLog(@"LOG: request");
        
        NSURLConnection* theConnection= nil;
        theConnection =[[NSURLConnection alloc] initWithRequest:postRequest delegate:self];
        [theConnection release];
        [postRequest release];
        
	}
	else if ([postType isEqual: @"cookieLogin"])
	{
        /*****************************************************
         * WebSSO Formpost Authentication using Cookies
         *****************************************************/
        NSLog(@"LOG: WebSSO Formpost Authentication using Cookies");
		NSLog(@"LOG: Cookie form post");
		NSString* keychainCookie =[command.arguments objectAtIndex:3];
		NSLog(@"LOG: cookieHeaderAll2 %@",self.cookieHeaderAll);
        
		request = [[[NSMutableURLRequest alloc] init] autorelease];
        [request setHTTPShouldHandleCookies:NO];
        [request setHTTPShouldUsePipelining:NO];
        if(self.webSSODomainString == nil)
        {
            self.webSSODomainString = @"sso.sp.edu.sg";
        }
        NSRange range = [webSSODomainString rangeOfString: @"testsp"];
        if (range.location == NSNotFound){
            /*****************************************************
             * Production server Form Post Url
             *****************************************************/
            [request setURL:[NSURL URLWithString:@"https://sso.sp.edu.sg/pkmslogin.form"]];
        }else {
            /*****************************************************
             * Test server Form Post Url
             *****************************************************/
            NSString* webSSOURL = [NSString stringWithFormat:@"https://%@/pkmslogin.form", webSSODomainString];
            [request setURL:[NSURL URLWithString:webSSOURL]];
        }
        
		[request setHTTPMethod:@"POST"];
		[request setValue:keychainCookie forHTTPHeaderField:@"Cookie"];
        NSURLConnection* theConnection=[[NSURLConnection alloc] initWithRequest:request delegate:self];
	}
    
    /********************
     CDVPluginResult* pluginResult = nil;
     NSString* echo = [command.arguments objectAtIndex:0];
     
     if (echo != nil && [echo length] > 0) {
     pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:echo];
     } else {
     pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
     }
     
     [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
     *******************/
}




/*****************************************************
 *
 * Sets the WebSSO Domain Url (Ex: 'mobileweb.sp.edu.sg)
 *
 * @param {String} [arguments objectAtIndex:1] Type
 *
 *****************************************************/
- (void)setWebSSODomain:(CDVInvokedUrlCommand*)command
{
    NSLog(@"LOG: setWebSSODomain");
    self.webSSODomainString = [command.arguments objectAtIndex:0];
     NSLog(@"LOG: setWebSSODomain %@",self.webSSODomainString);
}

/*****************************************************
 * The delegate will receive no further messages for the
 * connection and the NSURLConnection object can be released.
 *****************************************************/
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    
    
    /*
     pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:echo];
     
     pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
     
     
     [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
     */
    
    NSLog(@"LOG: connectionDidFinishLoading");
    self.responsetext = [[[NSString alloc] initWithData:receivedData encoding: NSASCIIStringEncoding] autorelease];
    
    if ([self.postType isEqual: @"type3"])
    {
        self.responsetext = [[[NSString alloc] initWithData:receivedData encoding: NSASCIIStringEncoding] autorelease];
        
        if (responsetext != nil)
        {
            /*****************************************************
             * Plugin Success Callback
             *****************************************************/
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:responsetext];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:self.callbackID];
        }else
        {
            /*****************************************************
             * Plugin Failure Callback
             *****************************************************/
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:self.callbackID];
        }
	}
}

/*****************************************************
 * Handle Http Connection Receive Data
 *****************************************************/
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [receivedData appendData:data];
}

/*****************************************************
 * Handle Http Authentication Challenge accordingly
 *****************************************************/
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

/*****************************************************
 * Handle After Recieving Http Authentication Challenge
 *****************************************************/
- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
}

/*****************************************************
 * Handle the Http Redirect Callback
 *****************************************************/
- (NSURLRequest *)connection: (NSURLConnection *)inConnection
             willSendRequest: (NSURLRequest *)inRequest
            redirectResponse: (NSURLResponse *)inRedirectResponse;
{
	
	NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)inRedirectResponse;
    /*****************************************************
     * Gets the Http Status Code
     *****************************************************/
    int statusCode = [httpResponse statusCode];
    NSLog(@"LOG: CODE %d",statusCode);
	
    /*****************************************************
     * Handle Http Redirect 301/302
     *****************************************************/
    if (inRedirectResponse) {
        NSLog(@"Handle Http Redirect 301/302");
		if ([inRedirectResponse respondsToSelector:@selector(allHeaderFields)]) {
			NSDictionary *dictionary = [inRedirectResponse allHeaderFields];
			NSLog([dictionary description]);
            
			/*****************************************************
             * WebSSO Login Authentication based on credentials
             *****************************************************/
			if ([self.postType isEqual: @"credentialsLogin"])
			{
                /*****************************************************
                 * Gets all the cookie in the response Header "Set-Cookie" and store in a variable
                 *****************************************************/
                self.cookieHeaderTemp01 = [NSString stringWithFormat:@"%@",[dictionary objectForKey:@"Set-Cookie"]];
                NSLog(@"LOG: self.cookieHeaderTemp01 %@",self.cookieHeaderTemp01);
                NSArray *myCookies = [self.cookieHeaderTemp01 componentsSeparatedByString:@";"];
                NSArray *myInnerCookies = [self.cookieHeaderTemp01 componentsSeparatedByString:@"="];
                NSString *myCookiesFullValue = [myCookies objectAtIndex:0];
                NSString *myCookiesName = [myInnerCookies objectAtIndex:0];
                
                NSString *myCookiesReplace = [myCookiesName stringByAppendingString:@"="];
                NSString * myCookiesValue = [myCookiesFullValue stringByReplacingOccurrencesOfString:myCookiesReplace withString:@""];
                
                NSMutableDictionary *cookieProperties = [NSMutableDictionary dictionary];
                [cookieProperties setObject:myCookiesName forKey:NSHTTPCookieName];
                [cookieProperties setObject:myCookiesValue forKey:NSHTTPCookieValue];
                [cookieProperties setObject:webSSODomainString forKey:NSHTTPCookieDomain];
                [cookieProperties setObject:webSSODomainString forKey:NSHTTPCookieOriginURL];
                [cookieProperties setObject:@"/" forKey:NSHTTPCookiePath];
                [cookieProperties setObject:@"0" forKey:NSHTTPCookieVersion];
                [cookieProperties setObject:[[NSDate date] dateByAddingTimeInterval:2629743] forKey:NSHTTPCookieExpires];
                NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
                [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
                
                 [cookieProperties setObject:@"eliser.lib.sp.edu.sg" forKey:NSHTTPCookieDomain];
                 [cookieProperties setObject:@"eliser.lib.sp.edu.sg" forKey:NSHTTPCookieOriginURL];
                 cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
                 [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
                 [cookieProperties setObject:@"m.lib.sp.edu.sg" forKey:NSHTTPCookieDomain];
                 [cookieProperties setObject:@"m.lib.sp.edu.sg" forKey:NSHTTPCookieOriginURL];
                 cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
                 [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
                 [cookieProperties setObject:@"esp.sp.edu.sg" forKey:NSHTTPCookieDomain];
                 [cookieProperties setObject:@"esp.lib.sp.edu.sg" forKey:NSHTTPCookieOriginURL];
                 cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
                 [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
                 
                
			}
            /*****************************************************
             * WebSSO Login Authentication based on session cookies
             *****************************************************/
			else if([self.postType isEqual: @"cookieLogin"])
			{
                /*****************************************************
                 * Gets the Response Http Header "Location"
                 *****************************************************/
                self.cookieHeaderAll = [NSString stringWithFormat:@"%@",[dictionary objectForKey:@"Location"]];
			}
		}
		return inRequest;
    } else {
        return inRequest;
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    /*****************************************************
     * Http Receive Data
     *****************************************************/
    NSLog(@"LOG: Http Receive Data");
    receivedData = [[NSMutableData alloc] init];
    
	NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
	NSLog(@"LOG: response %d", [httpResponse statusCode]);
    
	if ([response respondsToSelector:@selector(allHeaderFields)]) {
		NSDictionary *dictionary2 = [response allHeaderFields];
        /*****************************************************
         * WebSSO Login Authentication based on credentials
         *****************************************************/
		if([self.postType isEqual: @"credentialsLogin"])
		{
            /*****************************************************
             * Gets all the cookie in the response Header "Set-Cookie" and store in a variable
             *****************************************************/
            NSLog(@"LOG: Gets all the cookie in the response Header Set-Cookie and store in a variable");
            self.cookieHeaderTemp02 = [NSString stringWithFormat:@"%@",[dictionary2 objectForKey:@"Set-Cookie"]];
            NSLog(@"LOG: self.cookieHeaderTemp02 %@",self.cookieHeaderTemp02);
            /*****************************************************
             * Combines the session cookies in the redirect response and final response
             *****************************************************/
            self.cookieHeaderAll = [NSString stringWithFormat:@"%@;%@", self.cookieHeaderTemp01, self.cookieHeaderTemp02];
            /*****************************************************
             * Combines the session cookies in the redirect response and final response
             *****************************************************/
            self.cookieHeaderAll = [NSString stringWithFormat:@"%@;%@", self.cookieHeaderTemp01, self.cookieHeaderTemp02];
            NSArray *myCookies = [self.cookieHeaderTemp02 componentsSeparatedByString:@";"];
            NSArray *myInnerCookies = [self.cookieHeaderTemp02 componentsSeparatedByString:@"="];
            NSString *myCookiesFullValue = [myCookies objectAtIndex:0];
            NSString *myCookiesName = [myInnerCookies objectAtIndex:0];
            
            NSString *myCookiesReplace = [myCookiesName stringByAppendingString:@"="];
            NSString * myCookiesValue = [myCookiesFullValue stringByReplacingOccurrencesOfString:myCookiesReplace withString:@""];
            
            NSMutableDictionary *cookieProperties = [NSMutableDictionary dictionary];
            [cookieProperties setObject:myCookiesName forKey:NSHTTPCookieName];
            [cookieProperties setObject:myCookiesValue forKey:NSHTTPCookieValue];
            [cookieProperties setObject:webSSODomainString forKey:NSHTTPCookieDomain];
            [cookieProperties setObject:webSSODomainString forKey:NSHTTPCookieOriginURL];
            [cookieProperties setObject:@"/" forKey:NSHTTPCookiePath];
            [cookieProperties setObject:@"0" forKey:NSHTTPCookieVersion];
            [cookieProperties setObject:[[NSDate date] dateByAddingTimeInterval:2629743] forKey:NSHTTPCookieExpires];
            NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
             [cookieProperties setObject:@"eliser.lib.sp.edu.sg" forKey:NSHTTPCookieDomain];
             [cookieProperties setObject:@"eliser.lib.sp.edu.sg" forKey:NSHTTPCookieOriginURL];
             cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
             [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
             [cookieProperties setObject:@"m.lib.sp.edu.sg" forKey:NSHTTPCookieDomain];
             [cookieProperties setObject:@"m.lib.sp.edu.sg" forKey:NSHTTPCookieOriginURL];
             cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
             [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
             [cookieProperties setObject:@"esp.sp.edu.sg" forKey:NSHTTPCookieDomain];
             [cookieProperties setObject:@"esp.sp.edu.sg" forKey:NSHTTPCookieOriginURL];
             cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
             [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie]; 
		} else if([self.postType isEqual: @"cookieLogin"])
        {
            NSLog(@">>>>>>>>>>LOG: self.cookieStored %@",self.cookieStored);
            NSArray *myCookies = [self.cookieStored componentsSeparatedByString:@";"];
            NSArray *myInnerCookies = [self.cookieStored componentsSeparatedByString:@"="];
            NSString *myCookiesFullValue = [myCookies objectAtIndex:0];
            NSString *myCookiesName = [myInnerCookies objectAtIndex:0];
            
            NSString *myCookiesReplace = [myCookiesName stringByAppendingString:@"="];
            NSString * myCookiesValue = [myCookiesFullValue stringByReplacingOccurrencesOfString:myCookiesReplace withString:@""];
            
            NSMutableDictionary *cookieProperties = [NSMutableDictionary dictionary];
            [cookieProperties setObject:myCookiesName forKey:NSHTTPCookieName];
            [cookieProperties setObject:myCookiesValue forKey:NSHTTPCookieValue];
            [cookieProperties setObject:webSSODomainString forKey:NSHTTPCookieDomain];
            [cookieProperties setObject:webSSODomainString forKey:NSHTTPCookieOriginURL];
            [cookieProperties setObject:@"/" forKey:NSHTTPCookiePath];
            [cookieProperties setObject:@"0" forKey:NSHTTPCookieVersion];
            [cookieProperties setObject:[[NSDate date] dateByAddingTimeInterval:2629743] forKey:NSHTTPCookieExpires];
            NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
            
            [cookieProperties setObject:@"eliser.lib.sp.edu.sg" forKey:NSHTTPCookieDomain];
             [cookieProperties setObject:@"eliser.lib.sp.edu.sg" forKey:NSHTTPCookieOriginURL];
             cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
             [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
             [cookieProperties setObject:@"m.lib.sp.edu.sg" forKey:NSHTTPCookieDomain];
             [cookieProperties setObject:@"m.lib.sp.edu.sg" forKey:NSHTTPCookieOriginURL];
             cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
             [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
             [cookieProperties setObject:@"esp.sp.edu.sg" forKey:NSHTTPCookieDomain];
             [cookieProperties setObject:@"esp.lib.sp.edu.sg" forKey:NSHTTPCookieOriginURL];
             cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
             [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
             
        }
        
        
        
	}
    /*****************************************************
     * WebSSO Login Authentication based on credentials
     *****************************************************/
    if ([self.postType isEqual: @"credentialsLogin"])
    {
        /*****************************************************
         * Triggers back the session cookies back using phonegap plugin
         *****************************************************/
        
        if (self.cookieHeaderAll != nil)
        {
            /*****************************************************
             * Plugin Success Callback
             *****************************************************/
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:[self.cookieHeaderAll stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:self.callbackID];
            
        }else
        {
            /*****************************************************
             * Plugin Failure Callback
             *****************************************************/
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:self.callbackID];
        }
	}
    
    /*****************************************************
     * WebSSO Login Authentication based on session cookies
     *****************************************************/
    NSLog(@"LOG: WebSSO Login Authentication based on session cookies");
    if ([self.postType isEqual: @"cookieLogin"])
    {
        /*****************************************************
         * Triggers back the session cookies back using phonegap plugin
         *****************************************************/
        if (self.cookieHeaderAll != nil)
        {
            /*****************************************************
             * Plugin Success Callback
             *****************************************************/
            NSLog(@"LOG: WebSSO Plugin Success Callback");
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:[self.cookieHeaderAll stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:self.callbackID];
            
            
        }else
        {
            /*****************************************************
             * Plugin Failure Callback
             *****************************************************/
            NSLog(@"LOG: WebSSO Plugin Failure Callback");
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:self.callbackID];
        }
	}
    
    
    
}

@end
Ext.define('app.store.alertsSettingsStore', {
    extend: 'Ext.data.Store',


    requires: [
        'app.model.alertsSettingItemModel'
    ],

    config: {
        model: 'app.model.alertsSettingItemModel',
        autoLoad: true,
		data: [{index: '1', name: 'Logout'}]
      }     
        

});
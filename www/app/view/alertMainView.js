Ext.define('app.view.alertMainView', {
    extend: 'Ext.tab.Panel',
    xtype: 'alertMainView',

    requires: [
       'app.view.alertAboutView',
        'app.view.alertView'

   ],
    config: {
          id:  'alertMainView',
         tabBar: {
            docked: 'bottom'
         },
	     items: [
         {      

           xtype: 'alertView'

       }
        ,
         {

            xtype: 'alertAboutView'
        }
            
   			],
           
           listeners: {
           activeitemchange: function (tabPanel, tab, oldTab) {
           console.log("Tab: "+tab.config.title);
           
           PARAMS_REFRESHRETRIES = 0;
           PARAMS_TIMEOUTSTOPPED_REFRESH =false;
           try
           {
           window.clearTimeout(PARAMS_REFRESHMAINTIMEOUT);
           window.clearTimeout(PARAMS_REFRESHMAINTIMEOUTRETRY);
           window.clearTimeout(PARAMS_REFRESHMAINTIMEOUTCOMPLETED);
           }
           catch(err){}

           
           if(tab.config.title=="SP Alerts")
           {
           
           if (PARAMS_FROM_MESSAGEALERT == true) {
           PARAMS_ALERTLOCALSTORAGE = false;
           } else {
           PARAMS_ALERTLOCALSTORAGE = true;
           }
           
           if(PARAMS_ISBACKPRESSED==true){
           PARAMS_ALERTLOCALSTORAGE = true;
           }
           
           if(PARAMS_TIMEOUTSTOPPED_REFRESH==false)
           {
           PARAMS_FROM_MESSAGEALERT = true;
           }
           
           
           

           
           setTimeout(function(){processAlerts(
                                               '',
                                               '',
                                               '');}
                      ,500);
           Ext
           .getCmp(
                   'alertView')
           .animateActiveItem(
                              0,
                              {
                              type : 'slide',
                              direction : 'left'
                              });
           
           }
           
     
           
           
           }
           }

           

    }
     
});

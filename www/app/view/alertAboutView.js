Ext.define('app.view.alertAboutView', {
    extend: 'Ext.Panel',
    xtype: 'alertAboutView',
    
   requires: [
       'Ext.TitleBar'
   ],    config: {
        id: 'alertAboutView',
        title: 'About',
          iconCls: 'info',
        scrollable: 'vertical',
        styleHtmlContent: true,
	    style: 'background-color:#FFFFFF',
		
        items:[{
            xtype: 'titlebar',
            title: 'About',
            docked: 'top'
        }],
        listeners: {
            
            //	show: function(list, opts){
            	//       alert("show");
            	//   	Ext.getCmp("alertAboutView").setHtml("test");
            //	    },
            
        	initialize: function(){
        		   
                SP.OfflineCache.request(aboutURL, "", "", function(result){
                    //  Ext.getCmp("alertAboutView").setHtml(result);
  				   try {
  				   	Ext.getCmp("alertAboutView").setHtml(result);
  				   }
  				   catch(err){}
  				 
                  }, function(error){
  					
                  });
            	}
        }
    }
});

 //html: [
        /*       "<html><head><title>SP_AboutPage</title><meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'><meta name='GENERATOR' content='Rational� Application Developer for WebSphere� Software'></head><body><br/><center><img width='200px' src='sp_logo.png' /></center><div style='margin-left:10px; max-width:95%;text-align:left;font-size:14px;font-family:arial;'><b>About SP</b><br>Established in 1954, SP is Singapore's first polytechnic. Its ten academic schools with a student population of 16,000 offer 50 full-time diploma courses. Among SP's 160,000 graduates are successful entrepreneurs, top executives in multinational and public-listed corporations, and well-known professionals. <br><br></div></body></html>" */ var aboutURL = "http://fritzllanora.com/mobile/SP_AboutPage.html";
        //    ].join('') 
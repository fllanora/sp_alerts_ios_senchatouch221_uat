var PARAMS_ALERTLOCALSTORAGE = false;

var PARAMS_TOTAL_NEWUNREADALERTS = 0;
var ReadAlertOfflineStore;

var PARAMS_NEW_ALERTCOUNT = 0;
var PARAMS_TOTAL_UNREADALERTS = 0;
var PARAMS_TOTAL_NEWALERTS = 0;

var PARAMS_TOTAL_NEWUNREADALERTS = 0;

var PARAMS_REFRESH_FLAG = 0;

var collapsibleValue;

var OFFLINELAUNCH = true;

var PARAMS_OFFLINESHOWDATA_PROMPT = false;

var PARAMS_REFRESHMAINTIMEOUT;
var PARAMS_REFRESHMAINTIMEOUTRETRY;
var PARAMS_REFRESHMAINTIMEOUTCOMPLETED;
var PARAMS_REFRESHRETRIES = 0;
var PARAMS_TIMEOUTSTOPPED_REFRESH = false;

var PARAMS_ISBACKPRESSED = false;
//alert(screen.width); //768 // 320


Ext.define(
				'app.view.alertView',
				{
					extend : 'Ext.Panel',
					xtype : 'alertView',

					requires : [ 'Ext.TitleBar' ],

					config : {
						title : 'SP Alerts',
						id : 'alertView',
						iconCls : 'star',
						cls : 'alertPanel',
						autoDestroy : true,
						scrollable : null,
						style : 'background-color:#000000',
						layout : {
							type : 'card'
						},

						height : '100%',
						listeners : {

							// show: function(list, opts){
							// alert("show");
							// Ext.getCmp("alertAboutView").setHtml("test");
							// },

							initialize : function() {

								// alert("initialize");
								// Ext.getCmp(alertAboutView").setHtml("");

								Ext.define("readModel", {
									extend : "Ext.data.Model",
									config : {
										identifier : 'uuid',
										fields : [ {
											name : 'alert_Id',
											type : 'string'
										} ]
									}
								});

								ReadAlertOfflineStore = Ext.create(
										"Ext.data.Store", {
											id : 'ReadAlertOfflineStore',
											model : "readModel",
											autoLoad : true,
											autoSync : true,
											sorters : [ {
												property : 'alert_Source',
												direction : 'ASC'
											} ],
											proxy : {
												type : 'localstorage'
											}

										});

								try {
									SP.PushPlugin.registerToken(
											PARAMS_DEVICE_TOKEN,
											PARAMS_DEVICE_OS);
								} catch (err) {

								}

								/*
								 * SP.OfflineCache.request("http://fritzllanora.com/mobile/SP_AboutPage.html",
								 * "", "", function(result){ //
								 * Ext.getCmp("alertAboutView").setHtml(result);
								 * try {
								 * Ext.getCmp("alertAboutView").setHtml(result); }
								 * catch(err){}
								 *  }, function(error){
								 * 
								 * });
								 */

								SP.PushPlugin
										.getMessageId(
												[ "" ],
												function(value) {
													var messageID
													messageID = value;
													if ((messageID === null)
															|| (messageID === "")) {
														PARAMS_FROM_MESSAGEALERT = false;
														processAlerts('', '',
																'');

													} else {

														if (messageID == "101") {
															PARAMS_FROM_MESSAGEALERT = true;
															PARAMS_ALERTCATEGORYEXPANDED = "SP LIBRARY";
															processAlerts('',
																	'', '');
														} else {

															PARAMS_FROM_MESSAGEALERT = true;
															processMessageAlert(
																	'', '', '',
																	messageID);

															var rec;
															var checkedValue;
															var currentAlertID = messageID;

															try {
																rec = ReadAlertOfflineStore
																		.findRecord(
																				'alert_Id',
																				currentAlertID);
																checkedValue = rec
																		.get('alert_Id');
															} catch (err) {
															}

															console
																	.log(">>>>> checkedvalue: "
																			+ checkedValue);

															if (checkedValue != currentAlertID) {

																ReadAlertOfflineStore
																		.add({
																			alert_Id : currentAlertID
																		});
																console
																		.log("LOG: Read Alert status added to offline store");
															} else {
																console
																		.log("LOG: Read Alert status not added to offline store");
															}

															ReadAlertOfflineStore
																	.sync();

															SP.PushPlugin
																	.setMessageId(
																			[ "" ],
																			null,
																			null);
														}

													}

												}, function(error) {
												});

							}
						},

						items : [
								{
									xtype : 'panel',
									scrollable : null,
									id : 'alertListViewPanel',
									cls : 'alertListViewPanel',
									layout : 'fit',
									items : {
										xtype : 'panel',
										layout : 'vbox',
										items : [
												{
													xtype : 'titlebar',
													title : 'Alerts',
													docked : 'top',
													items : [
															{
																xtype : 'button',
																iconCls : 'refresh',
																iconMask : true,
																align : 'left',
																handler : function() {

																/*	Ext.Viewport
																			.setMasked({
																				xtype : 'loadmask',
																				message : 'Loading...'
																			});
*/
																	var networkState = navigator.connection.type;
																	var states = {};
																	states[Connection.UNKNOWN] = 'Unknown connection';
																	states[Connection.ETHERNET] = 'Ethernet connection';
																	states[Connection.WIFI] = 'WiFi connection';
																	states[Connection.CELL_2G] = 'Cell 2G connection';
																	states[Connection.CELL_3G] = 'Cell 3G connection';
																	states[Connection.CELL_4G] = 'Cell 4G connection';
																	states[Connection.CELL] = 'Cell generic connection';
																	states[Connection.NONE] = 'No network connection';
																	var checkConnection = states[networkState];
																	if ((checkConnection == 'Unknown connection')
																			|| (checkConnection == 'No network connection')) {
																		navigator.notification
																				.alert(
																						'Showing previously downloaded data.',
																						null,
																						'Offline',
																						'Ok');
																	}
                                                             
                                                                    PARAMS_REFRESHRETRIES = 0;
                                                                    PARAMS_TIMEOUTSTOPPED_REFRESH =false;
                                                             
																	PARAMS_ALERTCATEGORYEXPANDED_INDEX = 0;
																	PARAMS_ALERTCATEGORYEXPANDED = "OTHERS";
																	PARAMS_ALERTLOCALSTORAGE = false;
                                                             
                                                             try
                                                             {
                                                             window.clearTimeout(PARAMS_REFRESHMAINTIMEOUT);
                                                             window.clearTimeout(PARAMS_REFRESHMAINTIMEOUTRETRY);
                                                             window.clearTimeout(PARAMS_REFRESHMAINTIMEOUTCOMPLETED);
                                                             }
                                                             catch(err){}

																	processAlerts(
																			'',
																			'',
																			'');

																}
															},
															{
																xtype : 'button',
																iconMask : true,
																iconCls : 'settings',
																align : 'right',
																handler : function(
																		btn,
																		event) {

																	try {
																		ccaSettingsPanel
																				.destroy();
																	} catch (err) {
																	}

																	ccaSettingsPanel = Ext
																			.create(
																					'Ext.Panel',
																					{
																						id : 'settingsPanel',
																						hideOnMaskTap : true,
																						modal : true,
																						height : 52,
																						width : 117,
																						layout : {
																							type : 'fit',
																							pack : 'top',
																							align : 'stretch'
																						},
																						items : [ {
																							xtype : 'button',
																							text : 'Logout',
																							ui : 'decline',
																							height : 40,
																							width : 105,
																							handler : function() {
 
																								Ext
																										.getCmp(
																												'settingsPanel')
																										.hide();
                                                                                                 
                                                                                               try
                                                                                                 {
                                                                                                 Ext.getCmp('alertMainView').destroy();
                                                                                                 }
                                                                                                 catch(err){}
                                                                                                 

																								try {
																									SP.WebSSOPlugin
																											.clearUserData();
																								} catch (err) {
																								}

																								try {
																									Ext.Viewport
																											.removeAll(
																													true,
																													true);
																								} catch (err) {
																								}
         try
                                                                                                 {
																								Ext.Viewport
																										.add(Ext
																												.create('app.view.WebSSOLoginView'));
                                                                                                 }
                                                                                                 catch (err){}
                                                                                                
                                                                                                                                                                                                Ext.Viewport.setActiveItem(1);
                                                                                         
                                                                                                 
																								

																							}
																						} ],
																						left : 0,
																						padding : 10
																					});

																	ccaSettingsPanel
																			.showBy(btn);
																}
															} ]

												},
												{
													flex : 1,
													layout : 'vbox',
													items : [ {
														flex : 1.5
													}, {
														flex : 1,
														maxHeight : 15,
														maxHeight : 15,
														height : 15,
														id : 'alertSummaryText'
													} ]
												},
												{
													flex : 10,
													layout : 'hbox',
													items : [
															{
																flex : 0.7
															},
															{
																flex : 10,
																items : [ {
																	xtype : 'panel',
																	id : 'alertAccordionListView',
																	autoDestroy : true,
																	layout : {
																		type : 'accordion',
																		toggleOnTitlebar : true,
																		mode : 'SINGLE'
																	}
																} ]
															}, {
																flex : 0.7
															} ]
												}, {
													flex : 2,
													maxHeight : 25,
													maxHeight : 25,
													height : 25
												} ]
									}
								},
								{
									xtype : 'panel',
									html : '',
									id : 'alertDescription',
									styleHtmContent : true,
									layout : 'fit',
									items : [ {
										xtype : 'toolbar',
										title : 'Description',
										docked : 'top',
										items : [ {
											xtype : 'button',
											text : 'Back',
											ui : 'back',
											handler : function() {
												console
														.log("PARAMS_PUSHALERT_READ: ");

												Ext.getCmp('alertView')
														.animateActiveItem(0, {
															type : 'slide',
															direction : 'right'
														});
                                                
					                                                                            
                                                 if (PARAMS_FROM_MESSAGEALERT == true) {
													PARAMS_ALERTLOCALSTORAGE = false;
												} else {
													PARAMS_ALERTLOCALSTORAGE = true;
												}
                                                 
                                                 if(PARAMS_ISBACKPRESSED==true){
                                                 PARAMS_ALERTLOCALSTORAGE = true;
                                                 }
                                                 
                                                 if(PARAMS_ISBACKPRESSED==false)
                                                 {
                                                 PARAMS_ISBACKPRESSED = true;
                                                 }
                                                 
                                                 
                                                 PARAMS_REFRESHRETRIES = 0;
                                                 PARAMS_TIMEOUTSTOPPED_REFRESH =false;
                                                 try
                                                 {
                                                 window.clearTimeout(PARAMS_REFRESHMAINTIMEOUT);
                                                 window.clearTimeout(PARAMS_REFRESHMAINTIMEOUTRETRY);
                                                 window.clearTimeout(PARAMS_REFRESHMAINTIMEOUTCOMPLETED);
                                                 }
                                                 catch(err){}

                                                 
												processAlerts('', '', '');

											}
										} ]
									} ]

								} ]

					}
				});

/*******************************************************************************
 * Processing of Alerts
 ******************************************************************************/
function processAlerts(studentID, isBackUpdate, alertNotification) {

	var values = new Array();
	var arrayListAlerts = [];
	var arrayOfAlertHashObjects = [];
	PARAMS_NEW_ALERTCOUNT = 0;
	PARAMS_TOTAL_UNREADALERTS = 0;
	PARAMS_TOTAL_NEWALERTS = 0;

	PARAMS_TOTAL_NEWUNREADALERTS = 0;



	Ext.define("bModel", {
		extend : "Ext.data.Model",
		config : {
			identifier : 'uuid',
			fields : [ {
				name : 'alert_Id',
				type : 'string'
			}, {
				name : 'alert_Source',
				type : 'string'
			}, {
				name : 'alert_Title',
				type : 'string'
			}, {
				name : 'alert_TitleShort',
				type : 'string'
			}, {
				name : 'alert_Description',
				type : 'string'
			}, {
				name : 'xml_Status',
				type : 'string'
			}, {
				name : 'xml_Status_Code',
				type : 'string'
			}, {
				name : 'last_Updated',
				type : 'string'
			} ]
		}
	});

	try {
		console.log("ReadAlertOfflineStore Count: "
				+ ReadAlertOfflineStore.getCount());

	} catch (err) {
		console.log("error ReadAlertOfflineStore count");
	}

	var ListOfflineStore = Ext.create("Ext.data.Store", {
		id : 'AlertOfflineStoreLocalStorage',
		model : "bModel",
		autoLoad : true,
		sorters : [ {
			property : 'alert_Source',
			direction : 'ASC'
		} ],
		proxy : {
			type : 'localstorage'
		}
	});
/*
	Ext.Viewport.setMasked({
		xtype : 'loadmask',
		message : 'Loading...'
	});
 */

	if (OFFLINESUPPORT == true) {
		var networkState = navigator.connection.type;
		var states = {};
		states[Connection.UNKNOWN] = 'Unknown connection';
		states[Connection.ETHERNET] = 'Ethernet connection';
		states[Connection.WIFI] = 'WiFi connection';
		states[Connection.CELL_2G] = 'Cell 2G connection';
		states[Connection.CELL_3G] = 'Cell 3G connection';
		states[Connection.CELL_4G] = 'Cell 4G connection';
		states[Connection.CELL] = 'Cell generic connection';
		states[Connection.NONE] = 'No network connection';
		var checkConnection = states[networkState];
		if ((checkConnection == 'Unknown connection')
				|| (checkConnection == 'No network connection')) {
			Ext.Viewport.setMasked(false);

			PARAMS_ALERTLOCALSTORAGE = true;
			if (OFFLINELAUNCH == true) {
				setTimeout(function() {
					PARAMS_ALERTCATEGORYEXPANDED_INDEX = 0;
					PARAMS_ALERTCATEGORYEXPANDED = "OTHERS";
					PARAMS_ALERTLOCALSTORAGE = true;
					PARAMS_OFFLINESHOWDATA_PROMPT = true;
					processAlerts('', '', '');
					// alert("Showing previously downloaded data.");
					// Ext.Msg.alert("Offline.", "Showing previously downloaded
					// data.").doComponentLayout();

				}, 1000);
				OFFLINELAUNCH = false;
			}
		}
	}

	if (PARAMS_ALERTLOCALSTORAGE == true) {

		var values = null;
		var values = new Array();
		var tempAlertType;
		var arrayAlertsForAlertType = [];
		var firstTime = true;

		console.log("PARAMS_ALERTCATEGORYEXPANDED: "
				+ PARAMS_ALERTCATEGORYEXPANDED);

		Ext.AlertData = ListOfflineStore;
		var totalListOfflineStoreCount = 0;
        
        if(Ext.AlertData.getCount()==0)
        {
            Ext.Msg.alert("Message", "No Alerts");
            Ext.Viewport.setMasked(false);
        }


		Ext.AlertData
				.each(function(rec) {
 
                      
					var alertData = new Object();
					var alertTitleString = rec.get('alert_Title');
					var titleLength = alertTitleString.length;

                      if (screen.width >= 768) {
                      if (titleLength > 150) {
                      alertData["alert_TitleShort"] = alertTitleString
                      .substr(0, 140)
                      + "...";
                      } else {
                      alertData["alert_TitleShort"] = alertTitleString
                      .substr(0, 140);
                      }
                      } else {
                      if (titleLength > 45) {
                      alertData["alert_TitleShort"] = alertTitleString
                      .substr(0, 45)
                      + "...";
                      } else {
                      alertData["alert_TitleShort"] = alertTitleString
                      .substr(0, 45);
                      }
                      }
					var alertXmlStatus = rec.get('xml_Status');

					alertData["alert_Title"] = rec.get('alert_Title');
					alertData["alert_Id"] = rec.get('alert_Id');
					alertData["alert_Description"] = rec
							.get('alert_Description');
					alertData["xml_Status"] = rec.get('xml_Status');
					alertData["xml_Status_Code"] = rec.get('xml_Status_Code');
					alertData["last_Updated"] = rec.get('last_Updated');
					alertData["alert_Source"] = rec.get('alert_Source');

					try {
						ReadAlertOfflineStore.load();
					} catch (err) {

					}

					var findRecordData = "";
					var checkedValue = "";
					var currentAlertID = rec.get('alert_Id');

					try {
						findRecordData = ReadAlertOfflineStore.findRecord(
								'alert_Id', currentAlertID);
						checkedValue = findRecordData.get('alert_Id');
					} catch (err) {
					}

					if (checkedValue == currentAlertID) {

						alertData["alert_TitleShort"] = "<font color=#848484>"
								+ alertData["alert_TitleShort"] + "</font>";
						alertData["xml_Status"] = "READ";
						alertData["xml_Status_Code"] = "3";
						alertXmlStatus = "READ";
					}

					if (alertXmlStatus == 'UNREAD') {
						alertData["alert_TitleShort"] = "<b>"
								+ alertData["alert_TitleShort"] + "</b>";

					}
					if (alertXmlStatus == 'NEW') {

						if (screen.width >= 700) {

							if (titleLength > 65) {
								alertData["alert_TitleShort"] = "<div class='x-accordion-item-marker'></div><div class='x-accordion-item-markertext'><p><center>NEW</center></p></div>"
										+ "<div class='x-accordion-item-title'><b>"
										+ alertData["alert_TitleShort"]
										+ "</b></div>";
							} else {
								alertData["alert_TitleShort"] = "<div class='x-accordion-item-marker'></div><div class='x-accordion-item-markertextshort'><p><center>NEW</center></p></div>"
										+ "<div class='x-accordion-item-title'><b>"
										+ alertData["alert_TitleShort"]
										+ "</b></div>";

							}
						} else {
							if (titleLength > 23) {
								alertData["alert_TitleShort"] = "<div class='x-accordion-item-marker'></div><div class='x-accordion-item-markertext'><p><center>NEW</center></p></div>"
										+ "<div class='x-accordion-item-title'><b>"
										+ alertData["alert_TitleShort"]
										+ "</b></div>";
							} else {
								alertData["alert_TitleShort"] = "<div class='x-accordion-item-marker'></div><div class='x-accordion-item-markertextshortest'><p><center>NEW</center></p></div>"
										+ "<div class='x-accordion-item-title'><b>"
										+ alertData["alert_TitleShort"]
										+ "</b></div>";

							}
						}

					}

					if (firstTime) {
						arrayAlertsForAlertType.push(alertData);
					} else if (tempAlertType == rec.get('alert_Source')) {
						arrayAlertsForAlertType.push(alertData);
					}

					if (tempAlertType != rec.get('alert_Source')) {
						values.push(rec.get('alert_Source'));
						tempAlertType = rec.get('alert_Source');
						if (!firstTime) {

							arrayOfAlertHashObjects
									.push(arrayAlertsForAlertType);
							arrayAlertsForAlertType = [];
							arrayAlertsForAlertType.push(alertData);
						} else {
							firstTime = false;
						}
					}

					if (totalListOfflineStoreCount == (ListOfflineStore
							.getCount() - 1)) {
						arrayOfAlertHashObjects.push(arrayAlertsForAlertType);

                      
                      if(values.length>0)
                      {
                      try {
                      
                      if (PARAMS_REFRESH_FLAG == 0) {
                      
                      PARAMS_REFRESH_FLAG = 1;
                      } else {
                      Ext.getCmp('alertView').removeAll(true, true);
                      Ext
                      .getCmp('alertView')
                      .add(
                           {
                           xtype : 'panel',
                           scrollable : null,
                           id : 'alertListViewPanel',
                           cls : 'alertListViewPanel',
                           layout : 'fit',
                           items : {
                           xtype : 'panel',
                           layout : 'vbox',
                           items : [
                                    {
                                    xtype : 'titlebar',
                                    title : 'Alerts',
                                    docked : 'top',
                                    items : [
                                             {
                                             xtype : 'button',
                                             iconCls : 'refresh',
                                             iconMask : true,
                                             align : 'left',
                                             handler : function() {
                                             /*
                                              Ext.Viewport
                                              .setMasked({
                                              xtype : 'loadmask',
                                              message : 'Loading...'
                                              });
                                              */
                                             
                                             var networkState = navigator.connection.type;
                                             var states = {};
                                             states[Connection.UNKNOWN] = 'Unknown connection';
                                             states[Connection.ETHERNET] = 'Ethernet connection';
                                             states[Connection.WIFI] = 'WiFi connection';
                                             states[Connection.CELL_2G] = 'Cell 2G connection';
                                             states[Connection.CELL_3G] = 'Cell 3G connection';
                                             states[Connection.CELL_4G] = 'Cell 4G connection';
                                             states[Connection.CELL] = 'Cell generic connection';
                                             states[Connection.NONE] = 'No network connection';
                                             var checkConnection = states[networkState];
                                             if ((checkConnection == 'Unknown connection')
                                                 || (checkConnection == 'No network connection')) {
                                             navigator.notification
                                             .alert(
                                                    'Showing previously downloaded data.',
                                                    null,
                                                    'Offline',
                                                    'Ok');
                                             }
                                             
                                             PARAMS_REFRESHRETRIES = 0;
                                             PARAMS_TIMEOUTSTOPPED_REFRESH =false;
                                             
                                             PARAMS_ALERTCATEGORYEXPANDED_INDEX = 0;
                                             PARAMS_ALERTCATEGORYEXPANDED = "OTHERS";
                                             PARAMS_ALERTLOCALSTORAGE = false;
                                             
                                             try
                                             {
                                             window.clearTimeout(PARAMS_REFRESHMAINTIMEOUT);
                                             window.clearTimeout(PARAMS_REFRESHMAINTIMEOUTRETRY);
                                             window.clearTimeout(PARAMS_REFRESHMAINTIMEOUTCOMPLETED);
                                             }
                                             catch(err){}

                                             
                                             processAlerts(
                                                           '', '',
                                                           '');
                                             
                                             }
                                             },
                                             {
                                             xtype : 'button',
                                             iconMask : true,
                                             iconCls : 'settings',
                                             align : 'right',
                                             handler : function(
                                                                btn, event) {
                                             
                                             try {
                                             ccaSettingsPanel
                                             .destroy();
                                             } catch (err) {
                                             }
                                             
                                             ccaSettingsPanel = Ext
                                             .create(
                                                     'Ext.Panel',
                                                     {
                                                     id : 'settingsPanel',
                                                     hideOnMaskTap : true,
                                                     modal : true,
                                                     height : 52,
                                                     width : 117,
                                                     layout : {
                                                     type : 'fit',
                                                     pack : 'top',
                                                     align : 'stretch'
                                                     },
                                                     items : [ {
                                                              xtype : 'button',
                                                              text : 'Logout',
                                                              ui : 'decline',
                                                              height : 40,
                                                              width : 105,
                                                              handler : function() {
                                                              
                                                              Ext
                                                              .getCmp(
                                                                      'settingsPanel')
                                                              .hide();
                                                              
                                                              try
                                                              {
                                                              Ext.getCmp('alertMainView').destroy();
                                                              }
                                                              catch(err){}
                                                              
                                                              try {
                                                              SP.WebSSOPlugin
                                                              .clearUserData();
                                                              } catch (err) {
                                                              }
                                                              
                                                              try {
                                                              Ext.Viewport
                                                              .removeAll(
                                                                         true,
                                                                         true);
                                                              } catch (err) {
                                                              }
                                                              try
                                                              {
                                                              Ext.Viewport
                                                              .add(Ext
                                                                   .create('app.view.WebSSOLoginView'));
                                                              }
                                                              catch (err){}
                                                              
                                                              Ext.Viewport.setActiveItem(1);
                                                              
                                                              
                                                              
                                                              
                                                              
                                                              
                                                              
                                                              
                                                              }
                                                              } ],
                                                     left : 0,
                                                     padding : 10
                                                     });
                                             
                                             ccaSettingsPanel
                                             .showBy(btn);
                                             }
                                             } ]
                                    
                                    },
                                    {
                                    flex : 1,
                                    layout : 'vbox',
                                    items : [ {
                                             flex : 1.5
                                             }, {
                                             flex : 1,
                                             maxHeight : 15,
                                             maxHeight : 15,
                                             height : 15,
                                             id : 'alertSummaryText'
                                             } ]
                                    },
                                    {
                                    flex : 10,
                                    layout : 'hbox',
                                    items : [
                                             {
                                             flex : 0.7
                                             },
                                             {
                                             flex : 10,
                                             items : [ {
                                                      xtype : 'panel',
                                                      id : 'alertAccordionListView',
                                                      autoDestroy : true,
                                                      layout : {
                                                      type : 'accordion',
                                                      toggleOnTitlebar : true,
                                                      mode : 'SINGLE'
                                                      }
                                                      } ]
                                             }, {
                                             flex : 0.7
                                             } ]
                                    }, {
                                    flex : 1
                                    } ]
                           }
                           });
                      
                      Ext.getCmp('alertView').add({
                                                  xtype : 'panel',
                                                  html : '',
                                                  id : 'alertDescription',
                                                  layout : 'fit',
                                                  items : [ {
                                                           xtype : 'toolbar',
                                                           title : 'Description',
                                                           docked : 'top',
                                                           items : [ {
                                                                    xtype : 'button',
                                                                    text : 'Back',
                                                                    ui : 'back',
                                                                    handler : function() {
                                                                    console.log("PARAMS_PUSHALERT_READ: ");
                                                                    Ext.getCmp('alertView').animateActiveItem(0, {
                                                                                                              type : 'slide',
                                                                                                              direction : 'right'
                                                                                                              });
                                                                    
                                                                    if (PARAMS_FROM_MESSAGEALERT == true) {
                                                                    PARAMS_ALERTLOCALSTORAGE = false;
                                                                    } else {
                                                                    PARAMS_ALERTLOCALSTORAGE = true;
                                                                    }
                                                                    
                                                                    if(PARAMS_ISBACKPRESSED==true){
                                                                    PARAMS_ALERTLOCALSTORAGE = true;
                                                                    }
                                                                    
                                                                    if(PARAMS_ISBACKPRESSED==false)
                                                                    {
                                                                    PARAMS_ISBACKPRESSED = true;
                                                                    }
                                                                
                                                                    
                                                                    PARAMS_REFRESHRETRIES = 0;
                                                                    PARAMS_TIMEOUTSTOPPED_REFRESH =false;
                                                                    try
                                                                    {
                                                                    window.clearTimeout(PARAMS_REFRESHMAINTIMEOUT);
                                                                    window.clearTimeout(PARAMS_REFRESHMAINTIMEOUTRETRY);
                                                                    window.clearTimeout(PARAMS_REFRESHMAINTIMEOUTCOMPLETED);
                                                                    }
                                                                    catch(err){}

                                                                    processAlerts('', '', '');
                                                                    
                                                                    }
                                                                    } ]
                                                           } ]
                                                  
                                                  });
                      }
                      
                      try {
                      Ext.AlertData.removeAll();
                      } catch (err) {
                      }
                      
                      } catch (err) {
                      }
                      }
                      
						for ( var i = 0; i < values.length; i++) {

							var alertNewCount = 0;
							var alertUnreadCount = 0;

							var categoryHash = arrayOfAlertHashObjects[i];

							for ( var j = 0; j < categoryHash.length; j++) {

								if (categoryHash[j]["xml_Status"] == "NEW") {
									alertNewCount++;
								}
								if (categoryHash[j]["xml_Status"] == "UNREAD") {
									alertUnreadCount++;
								}

							}

							collapsibleValue = true;
							if (PARAMS_ALERTCATEGORYEXPANDED == values[i]) {
								PARAMS_ALERTCATEGORYEXPANDED_INDEX = i;
								collapsibleValue = false;
							}

							var totalUnreadNew = alertNewCount
									+ alertUnreadCount;
							console.log("alertNewCount: " + alertNewCount);
							console
									.log("alertUnreadCount: "
											+ alertUnreadCount);
							PARAMS_TOTAL_NEWALERTS = PARAMS_TOTAL_NEWALERTS
									+ alertNewCount;
							PARAMS_TOTAL_UNREADALERTS = PARAMS_TOTAL_UNREADALERTS
									+ alertUnreadCount;
							PARAMS_TOTAL_NEWUNREADALERTS = PARAMS_TOTAL_NEWUNREADALERTS
									+ alertNewCount + alertUnreadCount;
							console.log("totalUnreadNew: " + totalUnreadNew);

							var alertListHeight = 150;
							if (screen.width < 700) {
								alertListHeight = 150;
							} else if ((screen.width > 700)
									&& (screen.width < 800)) {
								alertListHeight = 200;
                      
                    		} else {
								alertListHeight = 400;
							}
                      
                            if(device.platform == 'iOS')
                           {
                           if(screen.width==768)
                           {
                           alertListHeight = 400;
                            }
                            }

							var alertList = Ext
									.create(
											"Ext.Panel",
											{
												title : values[i],
												unReadCount : totalUnreadNew,
												collapsed : collapsibleValue,
												expandedIndex : PARAMS_ALERTCATEGORYEXPANDED_INDEX,
												layout : 'hbox',
												items : [
														{
															width : 12
														},
														{
															xtype : 'panel',
															flex : 1,
															items : [ {
																xtype : 'list',
																height : alertListHeight,
																collapsed : collapsibleValue,
																cls : 'alertList',
																onItemDisclosure : false,
																scrollable : {
																	direction : 'vertical',
																	directionLock : true
																},
																itemTpl : '<div class="contact">{alert_TitleShort}</div>',
																store : {
																	model : 'bModel',
																	data : arrayOfAlertHashObjects[i],
																	sorters : [ {
																		property : 'xml_Status_Code',
																		direction : 'ASC'

																	} ]
																},
																listeners : {
																	itemtap : function(
																			list,
																			index,
																			element,
																			event) {
																		var currentRec = list
																				.getStore()
																				.getAt(
																						index);
																		var alertDesc = currentRec
																				.get('alert_Description');
																		var alertTitle = currentRec
																				.get('alert_Title');
																		console
																				.log("alertDesc :"
																						+ alertDesc);
																		console
																				.log("alertTitle :"
																						+ alertTitle);
																		console
																				.log("10");
																		PARAMS_ALERTCATEGORYEXPANDED = currentRec
																				.get('alert_Source');
																		console
																				.log("PARAMS_ALERTCATEGORYEXPANDED: "
																						+ PARAMS_ALERTCATEGORYEXPANDED);

																		var currentAlertID = currentRec
																				.get('alert_Id');

																		var value = "";
																		var rec = "";
																		var checkedValue = "";
																		try {
																			rec = ReadAlertOfflineStore
																					.findRecord(
																							'alert_Id',
																							currentAlertID);
																			checkedValue = rec
																					.get('alert_Id');
																		} catch (err) {
																		}

																		console
																				.log("checkedValue: "
																						+ checkedValue
																						+ " --- currentAlertID: "
																						+ currentAlertID);

																		if (checkedValue != currentAlertID) {
																			try {
																				ReadAlertOfflineStore
																						.add({

																							alert_Id : currentAlertID
																						});
																				ReadAlertOfflineStore
																						.sync();

																			} catch (err) {
																			}

																			console
																					.log("LOG: Read Alert status added to offline store");
																		} else {
																			console
																					.log("LOG: Read Alert status not added to offline store");
																		}
																		console
																				.log("ReadAlertOfflineStore Count: "
																						+ ReadAlertOfflineStore
																								.getCount());
																		var alertTitleContent = "<font size='4'><center><b>"
																				+ currentRec
																						.get('alert_Title')
																				+ "</center><b></font><br><br>";
																		var modifiedtextHtml = alertTitleContent
																				+ "<center>"
																				+ replaceURLWithHTMLLinks(alertDesc)
																				+ "</center>";
																		Ext
																				.getCmp(
																						'alertDescription')
																				.setHtml(
																						modifiedtextHtml);
																		Ext
																				.getCmp(
																						'alertView')
																				.animateActiveItem(
																						1,
																						{
																							type : 'slide',
																							direction : 'left'
																						});
																	}
																}
															} ]
														} ]

											});

							Ext.getCmp('alertSummaryText').setHtml(
									"<center><font size='2' color=#0d3d88><b>You have "
											+ PARAMS_TOTAL_NEWALERTS
											+ " new alerts and "
											+ PARAMS_TOTAL_UNREADALERTS
											+ " unread.</b></font> </center>");

							setTimeout(function() {
								Ext.getCmp('alertSummaryText').setHtml("");
							}, 5000);

							Ext.getCmp('alertAccordionListView').add(alertList);
							Ext.Viewport.setMasked(false);

							var networkState = navigator.connection.type;
							var states = {};
							states[Connection.UNKNOWN] = 'Unknown connection';
							states[Connection.ETHERNET] = 'Ethernet connection';
							states[Connection.WIFI] = 'WiFi connection';
							states[Connection.CELL_2G] = 'Cell 2G connection';
							states[Connection.CELL_3G] = 'Cell 3G connection';
							states[Connection.CELL_4G] = 'Cell 4G connection';
							states[Connection.CELL] = 'Cell generic connection';
							states[Connection.NONE] = 'No network connection';
							var checkConnection = states[networkState];
							if ((checkConnection == 'Unknown connection')
									|| (checkConnection == 'No network connection')) {

								if (PARAMS_OFFLINESHOWDATA_PROMPT == true) {
									setTimeout(
											function() {
												navigator.notification
														.alert(
																'Showing previously downloaded data.',
																null,
																'Offline', 'Ok');
											}, 2000);

									PARAMS_OFFLINESHOWDATA_PROMPT = false;
								}

							}

						} // values for loop
					}

					totalListOfflineStoreCount++;

				}); // end of ListOfflineStore Each
	} else {

		console.log("PARAMS_ALERTCATEGORYEXPANDED: "
				+ PARAMS_ALERTCATEGORYEXPANDED);
        
        //PARAMS_FROM_MESSAGEALERT = false;

		

		// var alertURL = "data/AlertRequest03.xml";
		// var alertURL = "http://localhost:8888/AlertRequest03.xml";
		//var alertURL = "http://www.fritzllanora.com/sp/AlertRequest03.xml";
		// var alertURL = "https://mobileweb.sp.edu.sg/AlertWS/student/alert/AlertRequest.jsp";
         var alertURL = "https://mobileweb-tst.testsf.testsp.edu.sg/AlertWS/student/alert/AlertRequest.jsp";

		// var alertURL =
		// "https://mobileweb.sp.edu.sg/AlertWS/student/alert/AlertRequest.jsp";

		Ext.AlertData = Ext.create("Ext.data.Store", {
			id : 'AlertStoreId',
			model : "bModel",
			autoLoad : true,
			sorters : [ {
				property : 'alert_Source',
				direction : 'ASC'
			} ],
			proxy : {
				type : "ajax",
				url : alertURL,
				reader : {
					type : 'xml',
					record : 'alerts'
				}
			}

		});
        
        

        
        if(PARAMS_REFRESHRETRIES==0)
        {
            Ext.Viewport.setMasked({
                                   xtype: 'loadmask',
                                   message: 'Connecting...'
                                   });

            
            PARAMS_REFRESHMAINTIMEOUT = setTimeout(function(){
                                        	if (PARAMS_TIMEOUTSTOPPED_REFRESH == false) {
                                            
                                            if (!isDiagnosticEnabled) {
                                            Ext.Viewport.setMasked({
                                                                   xtype: 'loadmask',
                                                                   message: 'Retrying(1)...'
                                                                   });
                                            
                                            PARAMS_REFRESHRETRIES = 1;
                                            PARAMS_ALERTLOCALSTORAGE = false;
                                            processAlerts(
                                                          '', '',
                                                          '');
                                            }                                            
                                            }
                                            },15000);
        }
        
        if(PARAMS_REFRESHRETRIES==1)
        {
            
            PARAMS_REFRESHMAINTIMEOUTRETRY = setTimeout(function(){
                                                 if (PARAMS_TIMEOUTSTOPPED_REFRESH == false) {
                                                 
                                                 if (!isDiagnosticEnabled) {
                                                 Ext.Viewport.setMasked({
                                                                        xtype: 'loadmask',
                                                                        message: 'Retrying(2)...'
                                                                        });
                                                 
                                                 PARAMS_REFRESHRETRIES = 2;
                                                 PARAMS_ALERTLOCALSTORAGE = false;
                                                 processAlerts(
                                                               '', '',
                                                               '');
                                                 }
                                                 }
                                                 },15000);

        }
        
        if(PARAMS_REFRESHRETRIES==2)
        {
            
            PARAMS_REFRESHMAINTIMEOUTCOMPLETED = setTimeout(function(){
     
                                                    if (!isDiagnosticEnabled) {
                                                     Ext.Viewport.setMasked(false);
                                                     }
                                                     if (PARAMS_TIMEOUTSTOPPED_REFRESH == false) {
                                                     Ext.Msg.alert("Connection Failed", "Connection Timeout");
                                                     PARAMS_TIMEOUTSTOPPED_REFRESH = true;
                                                    PARAMS_ALERTLOCALSTORAGE = false;
                                                     PARAMS_REFRESHRETRIES = 0;
                                                            try
                                                            {
                                                            window.clearTimeout(PARAMS_REFRESHMAINTIMEOUT);
                                                            window.clearTimeout(PARAMS_REFRESHMAINTIMEOUTRETRY);
                                                            window.clearTimeout(PARAMS_REFRESHMAINTIMEOUTCOMPLETED);
                                                            }
                                                            catch(err){}
                                                   // processAlerts('', '','');
                       
                                                     }
                                                     },15000);
        }

		Ext.AlertData.on('load',
                    //function(store, data, eOpts ) {
                    function(store, records, successful, operation, eOpts) {
                    
                    if(successful==true)
                    {
                         PARAMS_ISBACKPRESSED = true;
                         
                         if((store.getCount()==0))
                        {
                        Ext.Msg.alert("Message", "No Alerts");
                    		Ext.Viewport.setMasked(false);
                        }
                    PARAMS_TIMEOUTSTOPPED_REFRESH = true;
                    PARAMS_REFRESHRETRIES = 0;
                    try
                    {
                    window.clearTimeout(PARAMS_REFRESHMAINTIMEOUT);
                    window.clearTimeout(PARAMS_REFRESHMAINTIMEOUTRETRY);
                    window.clearTimeout(PARAMS_REFRESHMAINTIMEOUTCOMPLETED);
                    }
                    catch(err){}

                    }
                    
							var tempAlertType;
							var arrayAlertsForAlertType = [];
							var firstTime = true;
							/***************************************************
							 * Alert Data Parser
							 **************************************************/

                    var listOfflinestoreflag = false;
                    
							store.each(function(rec) {
										var alertData = new Object();
										var alertTitleString = rec
												.get('alert_Title');
										var titleLength = alertTitleString.length;
                                         PARAMS_TIMEOUTSTOPPED_REFRESH = true;
                                         
                                          if(listOfflinestoreflag==false)
                                          {
                                          
                                             ListOfflineStore.removeAll();
                                             ListOfflineStore.sync();
                                           listOfflinestoreflag = true;
                                          }
                    
                                          
										try {
											ListOfflineStore.add(rec.copy());
											ListOfflineStore.sync();
										} catch (err) {
										}

										if (screen.width >= 768) {
											if (titleLength > 150) {
												alertData["alert_TitleShort"] = alertTitleString
														.substr(0, 140)
														+ "...";
											} else {
												alertData["alert_TitleShort"] = alertTitleString
														.substr(0, 140);
											}
										} else {
											if (titleLength > 45) {
												alertData["alert_TitleShort"] = alertTitleString
														.substr(0, 45)
														+ "...";
											} else {
												alertData["alert_TitleShort"] = alertTitleString
														.substr(0, 45);
											}
										}
										var alertXmlStatus = rec
												.get('xml_Status');

										alertData["alert_Title"] = rec
												.get('alert_Title');
										alertData["alert_Id"] = rec
												.get('alert_Id');
										alertData["alert_Description"] = rec
												.get('alert_Description');
										alertData["xml_Status"] = rec
												.get('xml_Status');
										alertData["xml_Status_Code"] = rec
												.get('xml_Status_Code');
										alertData["last_Updated"] = rec
												.get('last_Updated');
										alertData["alert_Source"] = rec
												.get('alert_Source');

										try {
											ReadAlertOfflineStore.load();
										} catch (err) {

										}

										var findRecordData = "";
										var checkedValue = "";
										var currentAlertID = rec
												.get('alert_Id');

										try {
											findRecordData = ReadAlertOfflineStore
													.findRecord('alert_Id',
															currentAlertID);
											checkedValue = findRecordData
													.get('alert_Id');
										} catch (err) {
										}

										if (checkedValue == currentAlertID) {

											alertData["alert_TitleShort"] = "<font color=#848484>"
													+ alertData["alert_TitleShort"]
													+ "</font>";
											alertData["xml_Status"] = "READ";
											alertData["xml_Status_Code"] = "3";
											alertXmlStatus = "READ";
										}

										if (alertXmlStatus == 'UNREAD') {
											alertData["alert_TitleShort"] = "<b>"
													+ alertData["alert_TitleShort"]
													+ "</b>";

										}
										if (alertXmlStatus == 'NEW') {

											if (screen.width >= 700) {
												// alert(alertData["alert_TitleShort"]+"
												// = "+titleLength);
												// console.log("700 =
												// "+alertData["alert_TitleShort"]+"
												// = "+titleLength);
                                               if(device.platform=='iOS')
                                          {
                                          if (titleLength > 65) {
                                          alertData["alert_TitleShort"] = "<div class='x-accordion-item-marker'></div><div class='x-accordion-item-markertext'><p><center>NEW</center></p></div>"
                                          + "<div class='x-accordion-item-title'><b>"
                                          + alertData["alert_TitleShort"]
                                          + "</b></div>";
                                          } else {
                                          alertData["alert_TitleShort"] = "<div class='x-accordion-item-marker'></div><div class='x-accordion-item-markertextshort'><p><center>NEW</center></p></div>"
                                          + "<div class='x-accordion-item-title'><b>"
                                          + alertData["alert_TitleShort"]
                                          + "</b></div>";
                                          
                                          }

                                          }else
                                          {
												if (titleLength > 60) {
													alertData["alert_TitleShort"] = "<div class='x-accordion-item-marker'></div><div class='x-accordion-item-markertext'><p><center>NEW</center></p></div>"
															+ "<div class='x-accordion-item-title'><b>"
															+ alertData["alert_TitleShort"]
															+ "</b></div>";
												} else {
													alertData["alert_TitleShort"] = "<div class='x-accordion-item-marker'></div><div class='x-accordion-item-markertextshort'><p><center>NEW</center></p></div>"
															+ "<div class='x-accordion-item-title'><b>"
															+ alertData["alert_TitleShort"]
															+ "</b></div>";

												}
                                          }
											} else {
												// alert(alertData["alert_TitleShort"]+"
												// = "+titleLength);
												console
														.log(alertData["alert_TitleShort"]
																+ " = "
																+ titleLength);
												if (titleLength > 23) {
													alertData["alert_TitleShort"] = "<div class='x-accordion-item-marker'></div><div class='x-accordion-item-markertext'><p><center>NEW</center></p></div>"
															+ "<div class='x-accordion-item-title'><b>"
															+ alertData["alert_TitleShort"]
															+ "</b></div>";
												} else {
													alertData["alert_TitleShort"] = "<div class='x-accordion-item-marker'></div><div class='x-accordion-item-markertextshortest'><p><center>NEW</center></p></div>"
															+ "<div class='x-accordion-item-title'><b>"
															+ alertData["alert_TitleShort"]
															+ "</b></div>";

												}
											}

										}

										if (firstTime) {
											arrayAlertsForAlertType
													.push(alertData);
										} else if (tempAlertType == rec
												.get('alert_Source')) {
											arrayAlertsForAlertType
													.push(alertData);
										}
										if (tempAlertType != rec
												.get('alert_Source')) {
											values
													.push(rec
															.get('alert_Source'));
											tempAlertType = rec
													.get('alert_Source');
											if (!firstTime) {
												arrayOfAlertHashObjects
														.push(arrayAlertsForAlertType);
												arrayAlertsForAlertType = [];
												arrayAlertsForAlertType
														.push(alertData);
											} else {
												firstTime = false;
											}
										}

									}); // alertdata each
							if (arrayAlertsForAlertType != []) {
								arrayOfAlertHashObjects
										.push(arrayAlertsForAlertType);
							}

							console.log("values.length: " + values.length);
							console.log("arrayOfAlertHashObjects.length: "
									+ arrayOfAlertHashObjects.length); // arrayOfAlertHashObjects
                    
                    if(values.length>0)
                    {
                    try {
                    
                    if (PARAMS_REFRESH_FLAG == 0) {
                    
                    PARAMS_REFRESH_FLAG = 1;
                    } else {
                    Ext.getCmp('alertView').removeAll(true, true);
                    Ext
					.getCmp('alertView')
					.add(
                         {
                         xtype : 'panel',
                         scrollable : null,
                         id : 'alertListViewPanel',
                         cls : 'alertListViewPanel',
                         layout : 'fit',
                         items : {
                         xtype : 'panel',
                         layout : 'vbox',
                         items : [
                                  {
                                  xtype : 'titlebar',
                                  title : 'Alerts',
                                  docked : 'top',
                                  items : [
                                           {
                                           xtype : 'button',
                                           iconCls : 'refresh',
                                           iconMask : true,
                                           align : 'left',
                                           handler : function() {
                                           /*
                                            Ext.Viewport
                                            .setMasked({
                                            xtype : 'loadmask',
                                            message : 'Loading...'
                                            });
                                            */
                                           
                                           var networkState = navigator.connection.type;
                                           var states = {};
                                           states[Connection.UNKNOWN] = 'Unknown connection';
                                           states[Connection.ETHERNET] = 'Ethernet connection';
                                           states[Connection.WIFI] = 'WiFi connection';
                                           states[Connection.CELL_2G] = 'Cell 2G connection';
                                           states[Connection.CELL_3G] = 'Cell 3G connection';
                                           states[Connection.CELL_4G] = 'Cell 4G connection';
                                           states[Connection.CELL] = 'Cell generic connection';
                                           states[Connection.NONE] = 'No network connection';
                                           var checkConnection = states[networkState];
                                           if ((checkConnection == 'Unknown connection')
                                               || (checkConnection == 'No network connection')) {
                                           navigator.notification
                                           .alert(
                                                  'Showing previously downloaded data.',
                                                  null,
                                                  'Offline',
                                                  'Ok');
                                           }
                                           
                                           PARAMS_REFRESHRETRIES = 0;
                                           PARAMS_TIMEOUTSTOPPED_REFRESH =false;
                                           
                                           PARAMS_ALERTCATEGORYEXPANDED_INDEX = 0;
                                           PARAMS_ALERTCATEGORYEXPANDED = "OTHERS";
                                           PARAMS_ALERTLOCALSTORAGE = false;
                                           
                                           processAlerts(
                                                         '', '',
                                                         '');
                                           
                                           }
                                           },
                                           {
                                           xtype : 'button',
                                           iconMask : true,
                                           iconCls : 'settings',
                                           align : 'right',
                                           handler : function(
                                                              btn, event) {
                                           
                                           try {
                                           ccaSettingsPanel
                                           .destroy();
                                           } catch (err) {
                                           }
                                           
                                           ccaSettingsPanel = Ext
                                           .create(
                                                   'Ext.Panel',
                                                   {
                                                   id : 'settingsPanel',
                                                   hideOnMaskTap : true,
                                                   modal : true,
                                                   height : 52,
                                                   width : 117,
                                                   layout : {
                                                   type : 'fit',
                                                   pack : 'top',
                                                   align : 'stretch'
                                                   },
                                                   items : [ {
                                                            xtype : 'button',
                                                            text : 'Logout',
                                                            ui : 'decline',
                                                            height : 40,
                                                            width : 105,
                                                            handler : function() {
                                                            
                                                            Ext
                                                            .getCmp(
                                                                    'settingsPanel')
                                                            .hide();
                                                            try
                                                            {
                                                            Ext.getCmp('alertMainView').destroy();
                                                            }
                                                            catch(err){}
                                                            
                                                            try {
                                                            SP.WebSSOPlugin
                                                            .clearUserData();
                                                            } catch (err) {
                                                            }
                                                            
                                                            try {
                                                            Ext.Viewport
                                                            .removeAll(
                                                                       true,
                                                                       true);
                                                            } catch (err) {
                                                            }
                                                            try
                                                            {
                                                            Ext.Viewport
                                                            .add(Ext
                                                                 .create('app.view.WebSSOLoginView'));
                                                            }
                                                            catch (err){}
                                                            
                                                            Ext.Viewport.setActiveItem(1);
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            }
                                                            } ],
                                                   left : 0,
                                                   padding : 10
                                                   });
                                           
                                           ccaSettingsPanel
                                           .showBy(btn);
                                           }
                                           } ]
                                  
                                  },
                                  {
                                  flex : 1,
                                  layout : 'vbox',
                                  items : [ {
                                           flex : 1.5
                                           }, {
                                           flex : 1,
                                           maxHeight : 15,
                                           maxHeight : 15,
                                           height : 15,
                                           id : 'alertSummaryText'
                                           } ]
                                  },
                                  {
                                  flex : 10,
                                  layout : 'hbox',
                                  items : [
                                           {
                                           flex : 0.7
                                           },
                                           {
                                           flex : 10,
                                           items : [ {
                                                    xtype : 'panel',
                                                    id : 'alertAccordionListView',
                                                    autoDestroy : true,
                                                    layout : {
                                                    type : 'accordion',
                                                    toggleOnTitlebar : true,
                                                    mode : 'SINGLE'
                                                    }
                                                    } ]
                                           }, {
                                           flex : 0.7
                                           } ]
                                  }, {
                                  flex : 1
                                  } ]
                         }
                         });
                    
                    Ext.getCmp('alertView').add({
                                                xtype : 'panel',
                                                html : '',
                                                id : 'alertDescription',
                                                layout : 'fit',
                                                items : [ {
                                                         xtype : 'toolbar',
                                                         title : 'Description',
                                                         docked : 'top',
                                                         items : [ {
                                                                  xtype : 'button',
                                                                  text : 'Back',
                                                                  ui : 'back',
                                                                  handler : function() {
                                                                  console.log("PARAMS_PUSHALERT_READ: ");
                                                                  Ext.getCmp('alertView').animateActiveItem(0, {
                                                                                                            type : 'slide',
                                                                                                            direction : 'right'
                                                                                                            });
                                                                  
                                                                  if (PARAMS_FROM_MESSAGEALERT == true) {
                                                                  PARAMS_ALERTLOCALSTORAGE = false;
                                                                  } else {
                                                                  PARAMS_ALERTLOCALSTORAGE = true;
                                                                  }
                                                                  
                                                                  if(PARAMS_ISBACKPRESSED==true){
                                                                  PARAMS_ALERTLOCALSTORAGE = true;
                                                                  }
                                                                  
                                                                  if(PARAMS_ISBACKPRESSED==false)
                                                                  {
                                                                  PARAMS_ISBACKPRESSED = true;
                                                                  }
                                                                  
                                                                  PARAMS_REFRESHRETRIES = 0;
                                                                  PARAMS_TIMEOUTSTOPPED_REFRESH =false;
                                                                  try
                                                                  {
                                                                  window.clearTimeout(PARAMS_REFRESHMAINTIMEOUT);
                                                                  window.clearTimeout(PARAMS_REFRESHMAINTIMEOUTRETRY);
                                                                  window.clearTimeout(PARAMS_REFRESHMAINTIMEOUTCOMPLETED);
                                                                  }
                                                                  catch(err){}

                                                                  
                                                                  processAlerts('', '', '');
                                                                  
                                                                  }
                                                                  } ]
                                                         } ]
                                                
                                                });
                    }
                    
                    try {
                    Ext.AlertData.removeAll();
                    } catch (err) {
                    }
                    
                    } catch (err) {
                    }
                    }
							/***************************************************
							 * Alert Data List
							 **************************************************/
							for ( var i = 0; i < values.length; i++) {

								console.log("count: "
										+ arrayOfAlertHashObjects[i].length);
								console.log("Values : " + values[i]);

								var alertNewCount = 0;
								var alertUnreadCount = 0;

								var categoryHash = arrayOfAlertHashObjects[i];

								for ( var j = 0; j < categoryHash.length; j++) {

									if (categoryHash[j]["xml_Status"] == "NEW") {
										alertNewCount++;
									}
									if (categoryHash[j]["xml_Status"] == "UNREAD") {
										alertUnreadCount++;
									}

								}

								collapsibleValue = true;
								if (PARAMS_ALERTCATEGORYEXPANDED == values[i]) {
									PARAMS_ALERTCATEGORYEXPANDED_INDEX = i;
									collapsibleValue = false;

								}

								var totalUnreadNew = alertNewCount
										+ alertUnreadCount;
								console.log("alertNewCount: " + alertNewCount);
								console.log("alertUnreadCount: "
										+ alertUnreadCount);
								PARAMS_TOTAL_NEWALERTS = PARAMS_TOTAL_NEWALERTS
										+ alertNewCount;
								PARAMS_TOTAL_UNREADALERTS = PARAMS_TOTAL_UNREADALERTS
										+ alertUnreadCount;
								PARAMS_TOTAL_NEWUNREADALERTS = PARAMS_TOTAL_NEWUNREADALERTS
										+ alertNewCount + alertUnreadCount;
								console
										.log("totalUnreadNew: "
												+ totalUnreadNew);

								var alertListHeight = 150;
								if (screen.width < 700) {
									alertListHeight = 150;
								} else if ((screen.width > 700)
										&& (screen.width < 800)) {
									alertListHeight = 200;
								} else {
									alertListHeight = 400;
								}

                    
                    if(device.platform == 'iOS')
                    {
                    if(screen.width==768)
                    {
                    alertListHeight = 400;
                    }
                    }
                    
								var alertList = Ext
										.create(
												"Ext.Panel",
												{
													title : values[i],
													unReadCount : totalUnreadNew,
													layout : 'hbox',
													collapsed : collapsibleValue,
													expandedIndex : PARAMS_ALERTCATEGORYEXPANDED_INDEX,
													items : [
															{
																width : 12
															},
															{
																xtype : 'panel',
																flex : 1,
																items : [ {
																	xtype : 'list',
																	height : alertListHeight,
																	collapsed : collapsibleValue,
																	cls : 'alertList',
																	onItemDisclosure : false,
																	scrollable : {
																		direction : 'vertical',
																		directionLock : true
																	},
																	itemTpl : '{alert_TitleShort}',
																	store : {
																		model : 'bModel',
																		data : arrayOfAlertHashObjects[i],
																		sorters : [ {
																			property : 'xml_Status_Code',
																			direction : 'ASC'

																		} ]

																	},
																	listeners : {
																		itemtap : function(
																				list,
																				index,
																				element,
																				event) {
																			var currentRec = list
																					.getStore()
																					.getAt(
																							index);
																			var alertDesc = currentRec
																					.get('alert_Description');
																			var alertTitle = currentRec
																					.get('alert_Title');
																			console
																					.log("alertDesc :"
																							+ alertDesc);
																			console
																					.log("alertTitle :"
																							+ alertTitle);

																			PARAMS_ALERTCATEGORYEXPANDED = currentRec
																					.get('alert_Source');
																			console
																					.log("PARAMS_ALERTCATEGORYEXPANDED: "
																							+ PARAMS_ALERTCATEGORYEXPANDED);
																			PARAMS_ALERTCATEGORYEXPANDED_INDEX = currentRec
																					.get('alert_Source');

																			var currentAlertID = currentRec
																					.get('alert_Id');

																			var value = "";
																			var rec = "";
																			var checkedValue = "";

																			try {
																				rec = ReadAlertOfflineStore
																						.findRecord(
																								'alert_Id',
																								currentAlertID);
																				checkedValue = rec
																						.get('alert_Id');
																			} catch (err) {
																			}

																			console
																					.log("checkedValue: "
																							+ checkedValue
																							+ " --- currentAlertID: "
																							+ currentAlertID);

																			if (checkedValue != currentAlertID) {
																				try {
																					ReadAlertOfflineStore
																							.add({

																								alert_Id : currentAlertID
																							});
																					ReadAlertOfflineStore
																							.sync();
																				} catch (err) {
																				}

																				console
																						.log("LOG: Read Alert status added to offline store");
																			} else {
																				console
																						.log("LOG: Read Alert status not added to offline store");
																			}

																			console
																					.log("ReadAlertOfflineStore Count: "
																							+ ReadAlertOfflineStore
																									.getCount());

																			var alertTitleContent = "<font size='4'><center><b>"
																					+ currentRec
																							.get('alert_Title')
																					+ "</center><b></font><br><br>";
																			var modifiedtextHtml = alertTitleContent
																					+ "<center>"
																					+ replaceURLWithHTMLLinks(alertDesc)
																					+ "</center>";
																			Ext
																					.getCmp(
																							'alertDescription')
																					.setHtml(
																							modifiedtextHtml);
																			Ext
																					.getCmp(
																							'alertView')
																					.animateActiveItem(
																							1,
																							{
																								type : 'slide',
																								direction : 'left'
																							});

																		}
																	}
																} ]
															} ]

												});

							
                    
          
                    
                    Ext.getCmp('alertSummaryText').setHtml(
												"<center><font size='2' color=#0d3d88><b>You have "
														+ PARAMS_TOTAL_NEWALERTS
														+ " new alerts and "
														+ PARAMS_TOTAL_UNREADALERTS
														+ " unread.</b></font> </center>");
                    if(device.platform=='iOS')
                    {
                  
                    try{
                    SP.PushPlugin.setNotificationBadgeCount([""+PARAMS_TOTAL_NEWUNREADALERTS],function(result){PARAMS_TOTAL_NEWUNREADALERTS= 0; PARAMS_TOTAL_NEWALERTS= 0; PARAMS_TOTAL_UNREADALERTS=0;},function(error) {});
                    }
                    catch(err){}
                    }

								setTimeout(function() {
									Ext.getCmp('alertSummaryText').setHtml("");
								}, 5000);

								Ext.getCmp('alertAccordionListView').add(
										alertList);
								Ext.Viewport.setMasked(false);

							} // values for loop
						});

	}
    
    

	SP.PushPlugin.getMessageId([ "" ], function(value) {
		var messageID;
		messageID = value;
		if ((messageID === null) || (messageID === "")) {
			//PARAMS_FROM_MESSAGEALERT = false;

		}
	}, function(error) {
	});

}

function processMessageAlert(studentID, isBackUpdate, alertNotification,
		messageID) {

	var values = new Array();
	var arrayListAlerts = [];
	var arrayOfAlertHashObjects = [];
	PARAMS_NEW_ALERTCOUNT = 0;
	PARAMS_TOTAL_UNREADALERTS = 0;
	PARAMS_TOTAL_NEWALERTS = 0;

	PARAMS_TOTAL_NEWUNREADALERTS = 0;

	try {

		if (PARAMS_REFRESH_FLAG == 0) {

			PARAMS_REFRESH_FLAG = 1;
		} else {
			Ext.getCmp('alertView').removeAll(true, true);
			Ext
					.getCmp('alertView')
					.add(
							{
								xtype : 'panel',
								scrollable : null,
								id : 'alertListViewPanel',
								cls : 'alertListViewPanel',
								layout : 'fit',
								items : {
									xtype : 'panel',
									layout : 'vbox',
									items : [
											{
												xtype : 'titlebar',
												title : 'Alerts',
												docked : 'top',
												items : [
														{
															xtype : 'button',
															iconCls : 'refresh',
															iconMask : true,
															align : 'left',
															handler : function() {

																Ext.Viewport
																		.setMasked({
																			xtype : 'loadmask',
																			message : 'Loading...'
																		});

																var networkState = navigator.connection.type;
																var states = {};
																states[Connection.UNKNOWN] = 'Unknown connection';
																states[Connection.ETHERNET] = 'Ethernet connection';
																states[Connection.WIFI] = 'WiFi connection';
																states[Connection.CELL_2G] = 'Cell 2G connection';
																states[Connection.CELL_3G] = 'Cell 3G connection';
																states[Connection.CELL_4G] = 'Cell 4G connection';
																states[Connection.CELL] = 'Cell generic connection';
																states[Connection.NONE] = 'No network connection';
																var checkConnection = states[networkState];
																if ((checkConnection == 'Unknown connection')
																		|| (checkConnection == 'No network connection')) {
																	navigator.notification
																			.alert(
																					'Showing previously downloaded data.',
																					null,
																					'Offline',
																					'Ok');
																}
																PARAMS_ALERTCATEGORYEXPANDED_INDEX = 0;
																PARAMS_ALERTCATEGORYEXPANDED = "OTHERS";
																PARAMS_ALERTLOCALSTORAGE = false;

																processAlerts(
																		'', '',
																		'');

															}
														},
														{
															xtype : 'button',
															iconMask : true,
															iconCls : 'settings',
															align : 'right',
															handler : function(
																	btn, event) {

																try {
																	ccaSettingsPanel
																			.destroy();
																} catch (err) {
																}

																ccaSettingsPanel = Ext
																		.create(
																				'Ext.Panel',
																				{
																					id : 'settingsPanel',
																					hideOnMaskTap : true,
																					modal : true,
																					height : 52,
																					width : 117,
																					layout : {
																						type : 'fit',
																						pack : 'top',
																						align : 'stretch'
																					},
																					items : [ {
																						xtype : 'button',
																						text : 'Logout',
																						ui : 'decline',
																						height : 40,
																						width : 105,
																						handler : function() {
                                                                                             
                                                                                             Ext
                                                                                             .getCmp(
                                                                                                     'settingsPanel')
                                                                                             .hide();
                                                                                             
                                                                                             try
                                                                                             {
                                                                                             Ext.getCmp('alertMainView').destroy();
                                                                                             }
                                                                                             catch(err){}
                                                                                             
                                                                                             try {
                                                                                             SP.WebSSOPlugin
                                                                                             .clearUserData();
                                                                                             } catch (err) {
                                                                                             }
                                                                                             
                                                                                             try {
                                                                                             Ext.Viewport
                                                                                             .removeAll(
                                                                                                        true,
                                                                                                        true);
                                                                                             } catch (err) {
                                                                                             }
                                                                                             try
                                                                                             {
                                                                                             Ext.Viewport
                                                                                             .add(Ext
                                                                                                  .create('app.view.WebSSOLoginView'));
                                                                                             }
                                                                                             catch (err){}
                                                                                             
                                                                                             Ext.Viewport.setActiveItem(1);
                                                                                             
                                                                                             

																						}
																					} ],
																					left : 0,
																					padding : 10
																				});

																ccaSettingsPanel
																		.showBy(btn);
															}
														} ]

											},
											{
												flex : 1,
												layout : 'vbox',
												items : [ {
													flex : 1.5
												}, {
													flex : 1,
													maxHeight : 15,
													maxHeight : 15,
													height : 15,
													id : 'alertSummaryText'
												} ]
											},
											{
												flex : 10,
												layout : 'hbox',
												items : [
														{
															flex : 0.7
														},
														{
															flex : 10,
															items : [ {
																xtype : 'panel',
																id : 'alertAccordionListView',
																autoDestroy : true,
																layout : {
																	type : 'accordion',
																	toggleOnTitlebar : true,
																	mode : 'SINGLE'
																}
															} ]
														}, {
															flex : 0.7
														} ]
											}, {
												flex : 1
											} ]
								}
							});

			Ext.getCmp('alertView').add({
				xtype : 'panel',
				html : '',
				id : 'alertDescription',
				layout : 'fit',
				items : [ {
					xtype : 'toolbar',
					title : 'Description',
					docked : 'top',
					items : [ {
						xtype : 'button',
						text : 'Back',
						ui : 'back',
						handler : function() {
							console.log("PARAMS_PUSHALERT_READ: ");
							Ext.getCmp('alertView').animateActiveItem(0, {
								type : 'slide',
								direction : 'right'
							});
                             
                             PARAMS_ALERTLOCALSTORAGE = false;
                             
                             if (PARAMS_FROM_MESSAGEALERT == true) {
                             PARAMS_ALERTLOCALSTORAGE = false;
                             } else {
                             PARAMS_ALERTLOCALSTORAGE = true;
                             }
                             
                             if(PARAMS_ISBACKPRESSED==true){
                             PARAMS_ALERTLOCALSTORAGE = true;
                             }
                             
                             if(PARAMS_ISBACKPRESSED==false)
                             {
                             PARAMS_ISBACKPRESSED = true;
                             }

                             PARAMS_REFRESHRETRIES = 0;
                             PARAMS_TIMEOUTSTOPPED_REFRESH =false;
                             try
                             {
                             window.clearTimeout(PARAMS_REFRESHMAINTIMEOUT);
                             window.clearTimeout(PARAMS_REFRESHMAINTIMEOUTRETRY);
                             window.clearTimeout(PARAMS_REFRESHMAINTIMEOUTCOMPLETED);
                             }
                             catch(err){}

                              processAlerts('', '', '');

						}
					} ]
				} ]

			});
		}

		try {
			Ext.AlertData.removeAll();
		} catch (err) {
		}

	} catch (err) {
	}

	Ext.define("bModel", {
		extend : "Ext.data.Model",
		config : {
			identifier : 'uuid',
			fields : [ {
				name : 'alert_Id',
				type : 'string'
			}, {
				name : 'alert_Source',
				type : 'string'
			}, {
				name : 'alert_Title',
				type : 'string'
			}, {
				name : 'alert_TitleShort',
				type : 'string'
			}, {
				name : 'alert_Description',
				type : 'string'
			}, {
				name : 'xml_Status',
				type : 'string'
			}, {
				name : 'xml_Status_Code',
				type : 'string'
			}, {
				name : 'last_Updated',
				type : 'string'
			} ]
		}
	});

	var ListOfflineStore = Ext.create("Ext.data.Store", {
		id : 'AlertOfflineStoreLocalStorage',
		model : "bModel",
		autoLoad : true,
		sorters : [ {
			property : 'alert_Source',
			direction : 'ASC'
		} ],
		proxy : {
			type : 'localstorage'
		}
	});

	Ext.Viewport.setMasked({
		xtype : 'loadmask',
		message : 'Loading...'
	});

	ListOfflineStore.removeAll();
	ListOfflineStore.sync();

	// var alertURL = "data/AlertRequest03.xml";
	// var alertURL = "http://localhost:8888/AlertRequest03.xml";
	//var alertURL = "http://www.fritzllanora.com/sp/AlertRequest03.xml";
	// var alertURL = "https://mobileweb.sp.edu.sg/AlertWS/student/alert/AlertRequest.jsp";
	var alertURL = "https://mobileweb-tst.testsf.testsp.edu.sg/AlertWS/student/alert/AlertRequest.jsp";

	// var alertURL =
	// "https://mobileweb.sp.edu.sg/AlertWS/student/alert/AlertRequest.jsp";

	Ext.AlertData = Ext.create("Ext.data.Store", {
		id : 'AlertStoreId',
		model : "bModel",
		autoLoad : true,
		sorters : [ {
			property : 'alert_Source',
			direction : 'ASC'
		} ],
		proxy : {
			type : "ajax",
			url : alertURL,
			reader : {
				type : 'xml',
				record : 'alerts'
			}
		}

	});

	Ext.AlertData.on('load', function(store, records, successful, operation,
			eOpts) {
		var recMessageTitle = "";
		var recMessageDescription = "";

		try {
			var recMessage = store.findRecord('alert_Id', messageID);
			recMessageTitle = recMessage.get('alert_Title');
			recMessageDescription = recMessage.get('alert_Description');

		} catch (err) {
		}

		var alertTitleContent = "<font size='4'><center><b>" + recMessageTitle
				+ "</center><b></font><br><br>";
		var alertDesc = recMessageDescription;
		var modifiedtextHtml = alertTitleContent + "<center>"
				+ replaceURLWithHTMLLinks(alertDesc) + "</center>";
		Ext.getCmp('alertDescription').setHtml(modifiedtextHtml);
		Ext.getCmp('alertView').animateActiveItem(1, {
			type : 'slide',
			direction : 'left'
		});

		Ext.Viewport.setMasked(false);

	});

}

function openChildBrowserLink(url) {
	window.open(url, '_blank', 'location=yes');
}

/*****************************************************
 * Replace URL with HTML links
 *****************************************************/
function replaceURLWithHTMLLinks(text) {
	var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
	var expResult = text.replace(exp,
			"<a href=\"javascript:openChildBrowserLink('$1')\">$1</a>");
	return expResult;
}

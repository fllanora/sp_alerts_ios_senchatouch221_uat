/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // `load`, `deviceready`, `offline`, and `online`.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
	//	  document.addEventListener('resume', this.onResume, false);
    },
    // deviceready Event Handler
    //
    // The scope of `this` is the event. In order to call the `receivedEvent`
    // function, we must explicity call `app.receivedEvent(...);`
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
   // alert("started");
        console.log('Received Event: ' + id);
        // document.addEventListener("active", onResume, false);
		
	if (id == "deviceready") {
	
		SP.OfflineCache.initializeFileSystem();
		
		
		PARAMS_DEVICE_OS = device.platform;
		console.log("Device OS: " + PARAMS_DEVICE_OS);
		
		
		PARAMS_DEVICE_OS = PARAMS_DEVICE_OS.toLowerCase();
		
		if ((PARAMS_DEVICE_OS == 'ios') || (PARAMS_DEVICE_OS == 'ipad') || (PARAMS_DEVICE_OS == 'iphone') || (PARAMS_DEVICE_OS.indexOf('ipod') != -1)) {
			PARAMS_DEVICE_OS = 'iOS';
		}
		if (PARAMS_DEVICE_OS == 'android') {
			PARAMS_DEVICE_OS = 'Android';
		}
		if ((PARAMS_DEVICE_OS == 'win32nt') || (PARAMS_DEVICE_OS == 'wince')) {
			PARAMS_DEVICE_OS = 'Windows';
		}
		
		
		 var networkState = navigator.connection.type;
            var states = {};
            states[Connection.UNKNOWN] = 'Unknown connection';
            states[Connection.ETHERNET] = 'Ethernet connection';
            states[Connection.WIFI] = 'WiFi connection';
            states[Connection.CELL_2G] = 'Cell 2G connection';
            states[Connection.CELL_3G] = 'Cell 3G connection';
            states[Connection.CELL_4G] = 'Cell 4G connection';
            states[Connection.CELL] = 'Cell generic connection';
            states[Connection.NONE] = 'No network connection';
            
            var checkConnection = states[networkState];
                        
                    
                            if ((checkConnection == 'Unknown connection') ||
							(checkConnection == 'No network connection')) {
                            }
                            else{
					
								SP.PushPlugin.registerDevice(["PushPlugin", "push"], function(result){
									console.log("devicetoken: " + result);

									PARAMS_DEVICE_TOKEN = result;
								}, function(error){
								});
							}
    
              document.addEventListener("active", onResume, false);
    
       
	}
		
    }
};



function onResume() {
	
	SP.PushPlugin
	.getMessageId(
                  [ "" ],
                  function(value) {
                  var messageID
                  messageID = value;
                  if ((messageID === null)
                      || (messageID === "")) {
                  
                  PARAMS_FROM_MESSAGEALERT = false;
               //   processAlerts('', '',
                 //               '');
                  
                  } else {
                     
                  try
                  {
                  Ext.getCmp('alertMainView').setActiveItem(0);
                //  Ext.getCmp('alertMainView').animateActiveItem(0, {
                  //                                          type : 'slide',
                  //                                          direction : 'right'
                //                                            });
                  }
                  catch(err){}
                  PARAMS_ALERTLOCALSTORAGE = false;
                  
                  if (messageID == "101") {
                  Ext.Viewport
                  .setMasked({
                             xtype : 'loadmask',
                             message : 'Loading...'
                             });
                  PARAMS_FROM_MESSAGEALERT = true;
                  PARAMS_ALERTCATEGORYEXPANDED = "SP LIBRARY";
                  setTimeout(function(){processAlerts('', '',
                                                      '');},4500);
                  
                  } else {
                  
                  PARAMS_FROM_MESSAGEALERT = true;
                  PARAMS_ALERTLOCALSTORAGE = false;
                  processMessageAlert(
                                      '', '', '',
                                      messageID);
                  
                  var rec;
                  var checkedValue;
                  var currentAlertID = messageID;
                  
                  try {
                  rec = ReadAlertOfflineStore
                  .findRecord(
                              'alert_Id',
                              currentAlertID);
                  checkedValue = rec
                  .get('alert_Id');
                  } catch (err) {
                  }
                  
                  console
                  .log(">>>>> checkedvalue: "
                       + checkedValue);
                  
                  if (checkedValue != currentAlertID) {
                  
                  ReadAlertOfflineStore
                  .add({
                       alert_Id : currentAlertID
                       });
                  console
                  .log("LOG: Read Alert status added to offline store");
                  } else {
                  console
                  .log("LOG: Read Alert status not added to offline store");
                  }
                  
                  ReadAlertOfflineStore
                  .sync();
                  
                  SP.PushPlugin
                  .setMessageId(
                                [ "" ],
                                null,
                                null);
                  }
                  
                  }
                  
                  }, function(error) {
                  });
    
	
	
}



app.initialize();
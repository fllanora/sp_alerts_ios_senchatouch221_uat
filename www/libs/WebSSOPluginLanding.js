/*****************************************************
 * WebSSO Library
 *
 * LANDING PAGE
 *
 *****************************************************/
function WebSSOlandingPage(){
    if (isLogEnabled) {
        console.log("LOG: WebSSOlandingPage()");
    }
   
   try {
   	Ext.Viewport.removeAll(true, true);
   }
   catch(err){}
	Ext.Viewport.add(Ext.create('app.view.alertMainView'));
}

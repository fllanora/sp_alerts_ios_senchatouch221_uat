/*******************************************************
 * Variable Declarations
 ******************************************************/
var isDiagnosticEnabled = false;
var isLogEnabled = true;
var TimestampOfflineStore;

/******************************************************
 * OfflineCache Library
 *
 * Functions:
 *      SP.OfflineCache.initializeFileSystem()
 *      SP.OfflineCache.createCache(filename, contents)
 *      SP.OfflineCache.readCache(fileName, callback)
 *      SP.OfflineCache.clearCache(fileName)
 *      SP.OfflineCache.storeCache(onlineStore, offlineStore, listId)
 *      SP.OfflineCache.storeFilterCache(onlineStore, offlineStore, listId)
 *      SP.OfflineCache.pickerCache(onlineStore, offlineStore, pickerId)
 *      SP.OfflineCache.getFileSystemRootPath()
 *      SP.OfflineCache.updatePanelOfflineCache(panelOffline)
 *
 ******************************************************/
var SP = SP || {};

SP.OfflineCache = {
    /******************************************************
     * OfflineCache Variable Declarations
     ******************************************************/
    fileSystem: "",
    file: "",
    fileEntry: "",
    fileName: "",
	applicationPackageName: "",
	webSSOCookie: "",
	winIsoRootPath: "",
    /* end of OfflineCache Variable Declarations */
    
    /******************************************************
     * Initialize OfflineCache Library
     ******************************************************/
    initializeFileSystem: function(){
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, SP.OfflineCache.onFileSystemSuccess, SP.OfflineCache.fail);
      //  window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
  	},
    /* end of fileSystemReady */
    
    /******************************************************
     * Prefrom Ajax request and process the Offline Cache
     * @param {String} url
     * @param {String} manifestCacheUrl
     * @param {function} successCallback
     * @param {function} failCallback
     ******************************************************/
    request: function(url, manifestCacheUrl, webSSOCookie, successCallback, failCallback){
    
       // SP.OfflineCache.initializeFileSystem();
	  
	  //  cordova.exec(function(result){SP.OfflineCache.applicationPackageName = result;}, function(result){}, "OfflineCache", "getApplicationPackageName", [fileNameCache]);                           

	  
	   if(device.platform=='Win32NT')
			  {
       SP.OfflineCache.initializeFileSystem();
	     cordova.exec(function(result){
	               SP.OfflineCache.winIsoRootPath = result
              }, function(result){}, "OfflineCache", "getIsoRootPath",[""]);  
		}

        var fileNameCache = url;
		 fileNameCache = fileNameCache.replace("https://", "");
        fileNameCache = fileNameCache.replace("http://", "");
        fileNameCache = fileNameCache.replace(/\./g, "");
        fileNameCache = fileNameCache.replace(/\//g, "");
        fileNameCache = fileNameCache + ".cache";
		
		
		SP.OfflineCache.webSSOCookie = webSSOCookie;
		
		//PhoneGap.exec(function(result){SP.OfflineCache.applicationPackageName = result;}, function(result){}, "OfflineCache", "getApplicationPackageName", [fileNameCache]);                           
    var networkState = navigator.network.connection.type;
        var states = {};
        states[Connection.UNKNOWN] = 'Unknown connection';
        states[Connection.ETHERNET] = 'Ethernet connection';
        states[Connection.WIFI] = 'WiFi connection';
        states[Connection.CELL_2G] = 'Cell 2G connection';
        states[Connection.CELL_3G] = 'Cell 3G connection';
        states[Connection.CELL_4G] = 'Cell 4G connection';
        states[Connection.NONE] = 'No network connection';
        
        var checkConnection = states[networkState];
		
        if ((checkConnection== 'Unknown connection') || (checkConnection == 'No network connection')) {
            try {

			  if(device.platform=='Win32NT')
			  {
			   cordova.exec(function(result){
	               successCallback(result);
              }, function(result){}, "OfflineCache", "readOfflineCache",[fileNameCache]);  
			  }
			  else
			  {

                SP.OfflineCache.readCache(fileNameCache, function(result){
                    successCallback(result);
                });
				}
            } 
            catch (err) {
                failCallback("error");
                console.log("LOG: error read");
            }
            
        }
        else {
        
            if ((manifestCacheUrl == "") || (manifestCacheUrl == null)) {
                SP.OfflineCache.processCache(new Array(), url, fileNameCache, function(result){
                    successCallback(result);
                });
            }
            else {
            
                SP.OfflineCache.getCacheManifest(manifestCacheUrl, function(result){
                    SP.OfflineCache.processCache(result, url, fileNameCache, function(result){
                        successCallback(result);
                    });
                }, function(result){
                    failCallback("error");
                });
            }
            
        }
    },
    
    
    /******************************************************
     * Creates a file by which the cache is stored
     * @param {String} filename Name of the file
     * @param {String} contents Contents of the file
     ******************************************************/
    createCache: function(filename, contents){
        var writer = new FileWriter(SP.OfflineCache.getFileSystemRootPath() + "/" + filename);
        fileName = filename;
        writer.onload = function(evt){
            /** 
             * Cache successfully written
             */
            console.log(evt.target.result);
        };
        writer.onerror = SP.OfflineCache.fail;
        writer.write(contents);
        if (isLogEnabled) {
            console.log("LOG: Create File and set contents");
        }
    },
    /* end of createCache */
    
    /******************************************************
     * Reads Cache from file
     * @param {String} filename Name of the file
     * @param {Function} callback Callback function
     ******************************************************/
    readCache: function(fileName, callback){
      //  var filePath = SP.OfflineCache.getFileSystemRootPath() + "/" + fileName;
        SP.OfflineCache.fileSystem.root.getFile(fileName, null, SP.OfflineCache.readCacheFileEntry, SP.OfflineCache.fail);
        
        var reader = new FileReader();
	    reader.onloadend = function(evt) {
	        console.log("Read as text");
	        console.log(evt.target.result);
	        callback(evt.target.result);
	    };
	    
        reader.onerror = function(error){
            callback(error.code);
            if (isLogEnabled) {
                console.log("LOG: Error code: " + error.code);
            }
        };
        
	    reader.readAsText(SP.OfflineCache.file);
   },
    
    /* end of readCache */
    readCacheFileEntry: function(fileEntry) {
        fileEntry.file(SP.OfflineCache.readCacheFile, SP.OfflineCache.fail);
    },
    
   readCacheFile: function(file){
	   SP.OfflineCache.file = file;
    },
    

    
    /******************************************************
     * Deletes the Cache/File
     * @param {String} filename Name of the file
     * @param {Function} callback Callback function
     ******************************************************/
    clearCache: function(fileName, callback){
        var result = "";
        /** 
         * Get File
         */
        SP.OfflineCache.fileSystem.root.getFile(fileName, {
            create: false
        }, clearFileEntry, clearCacheFail);
        function clearFileEntry(fileEntry){
            /** 
             * Clear File
             */
            fileEntry.remove(clearCacheSuccess, clearCacheFail);
        }
        function clearCacheSuccess(evt){
            /** 
             * Clear Cache Successful
             */
            result = "success";
            callback(result);
            if (isLogEnabled) {
                console.log("LOG: Clear cache successful");
            }
        }
        function clearCacheFail(error){
            /** 
             * Clear Cache failed
             */
            result = "fail";
            callback(result);
            if (isLogEnabled) {
                console.log("LOG: Clear cache failed");
            }
        }
    },
    /* end of clearCache */
    
    /******************************************************
     * Cache the store and load the cache content if offline
     * @param {Store} onlineStore store
     * @param {Store} offlineStore store
     * @param {String} listId list id
     ******************************************************/
    storeCache: function(onlineStore, offlineStore, listId){
        /** 
         * Checks if there's network connection
         */
       var networkState = navigator.network.connection.type;
        var states = {};
        states[Connection.UNKNOWN] = 'Unknown connection';
        states[Connection.ETHERNET] = 'Ethernet connection';
        states[Connection.WIFI] = 'WiFi connection';
        states[Connection.CELL_2G] = 'Cell 2G connection';
        states[Connection.CELL_3G] = 'Cell 3G connection';
        states[Connection.CELL_4G] = 'Cell 4G connection';
        states[Connection.NONE] = 'No network connection';
        
        var checkConnection = states[networkState];
		
        if ((checkConnection== 'Unknown connection') || (checkConnection == 'No network connection')) {
            Ext.getCmp(listId).bindStore(offlineStore);
            if (Ext.getCmp(listId).rendered) {
                Ext.getCmp(listId).scroller.scrollTo({
                    x: 0,
                    y: 0
                });
            }
        }
        else {
            if (Ext.getCmp(listId).store == null) {
                /** 
                 * Binds the store and loads the data
                 */
                Ext.getCmp(listId).bindStore(onlineStore);
                offlineStore.getProxy().clear();
                offlineStore.loadRecords(onlineStore.data.items, false);
                offlineStore.sync();
            }
            if (Ext.getCmp(listId).rendered) {
                Ext.getCmp(listId).scroller.scrollTo({
                    x: 0,
                    y: 0
                });
            }
        }
    },
    /* end of storeCache */
    
    /******************************************************
     * Cache the Filter store and load the cache content if offline
     * @param {Store} onlineStore store
     * @param {Store} offlineStore store
     * @param {String} listId list id
     ******************************************************/
    storeFilterCache: function(onlineStore, offlineStore, listId){
        /** 
         * Checks if there's network connection
         */
        var networkState = navigator.network.connection.type;
        var states = {};
        states[Connection.UNKNOWN] = 'Unknown connection';
        states[Connection.ETHERNET] = 'Ethernet connection';
        states[Connection.WIFI] = 'WiFi connection';
        states[Connection.CELL_2G] = 'Cell 2G connection';
        states[Connection.CELL_3G] = 'Cell 3G connection';
        states[Connection.CELL_4G] = 'Cell 4G connection';
        states[Connection.NONE] = 'No network connection';
        
        var checkConnection = states[networkState];
		
        if ((checkConnection== 'Unknown connection') || (checkConnection == 'No network connection')) {     /** 
             * Binds the store and loads the data
             */
            Ext.getCmp(listId).bindStore(offlineStore);
            if (Ext.getCmp(listId).rendered) {
                Ext.getCmp(listId).scroller.scrollTo({
                    x: 0,
                    y: 0
                });
            }
        }
        else {
            /** 
             * Loads the records from the online store
             */
            offlineStore.loadRecords(onlineStore.data.items, false);
            offlineStore.sync();
            if (Ext.getCmp(listId).rendered) {
                Ext.getCmp(listId).scroller.scrollTo({
                    x: 0,
                    y: 0
                });
            }
        }
    },
    /* end of storeFilterCache */
    
    /******************************************************
     * Cache the Picker store and load the cache content if offline
     * @param {Store} onlineStore store
     * @param {Store} offlineStore store
     * @param {String} pickerId list id
     ******************************************************/
    pickerCache: function(onlineStore, offlineStore, pickerId){
        if ((checkConnection() == 'Unknown connection') || (checkConnection() == 'No network connection')) {
            Ext.getCmp(pickerId).items.first().bindStore(offlineStore);
        }
        else {
            if (Ext.getCmp(pickerId).items.first().getStore() == null) {
                /** 
                 * Binds the store and loads the data
                 */
                Ext.getCmp(pickerId).items.first().bindStore(onlineStore);
                offlineStore.getProxy().clear();
                offlineStore.loadRecords(onlineStore.data.items, false);
                offlineStore.sync();
            }
        }
    },
    /* end of pickerCache */
    
    /******************************************************
     * The phone filesystem is ready to be used
     * @param {Filesystem} FileSystem
     ******************************************************/
    onFileSystemSuccess: function(FileSystem){
        SP.OfflineCache.fileSystem = FileSystem;
        if (isLogEnabled) {
            console.log("LOG: Phone filesystem is ready.");
            console.log("LOG: FileSystemName: " + FileSystem.name);
            console.log("LOG: File System root path: " + FileSystem.root.fullPath);
        }
    },
    /* end of onFileSystemSuccess */
    
    /******************************************************
     * Gets the Application Package Name
     ******************************************************/
    getApplicationPackageName: function(){
		
        return SP.OfflineCache.applicationPackageName;
    },
    /* end of fileSystemRootPath */
	
	   
    /******************************************************
     * Gets the Root Path of the phone filesystem
     ******************************************************/
    getFileSystemRootPath: function(){
	var rootPath = "";
	 if(device.platform=='Win32NT')
			  {
	  rootPath =  SP.OfflineCache.winIsoRootPath;
	           
		}
		else {
		rootPath =  SP.OfflineCache.fileSystem.root.fullPath;
		}


        return rootPath;
    },
    /* end of fileSystemRootPath */
    
    /******************************************************
     * Gets the Root Path of the phone filesystem
     ******************************************************/
    getFileName: function(){
        return fileName;
    },
    /* end of fileSystemRootPath */
    
    /******************************************************
     * Updates the panel with the offline cache
     * @param {Panel Object} panelOffline Panel to be updated with the stored cache
     * @param {String} cache Cache stored
     ******************************************************/
    updatePanelOfflineCache: function(panelOffline, cache){
        panelOffline.update(cache);
        if (isLogEnabled) {
            console.log("LOG: Update Panel with Offline Cache");
        }
    },
    /* end of updatePanelOfflineCache */
    
    
    /******************************************************
     * Download the Cache Manifest files and process it
     * @param {String} manifestUrl Cache Manifest url
     * @param {function} successCallback
     * @param {function} failCallback
     ******************************************************/
    getCacheManifest: function(manifestUrl, successCallback, failCallback){
        /** 
         * Perform Ajax Request using the Manifest URL
         */
        Ext.Ajax.request({
            url: manifestUrl,
            success: function(response, opts){
                var outputStrManifest = response.responseText;
                var arraySelectedManifestCache = outputStrManifest.split(/\r\n|\r|\n/);
                successCallback(arraySelectedManifestCache);
            },
            failure: function(response, opts){
                failCallback("fail");
            }
        });
    },
    
    /******************************************************
     * Downloads and process the Cache
     * @param {Array} arrayManifestCache
     * @param {String} siteUrl
     * @param {String}  fileNameCache
     * @param {function} successCallback
     ******************************************************/
     processCache: function(arrayManifestCache, siteUrl, fileNameCache, successCallbackProcess){
      
	        Ext.regModel('timeStampModel', {
                                    fields: ['id', 'filenameurl', 'date']
                                });
                                              
               TimestampOfflineStore = new Ext.data.Store({
                            model: 'timeStampModel',
                            proxy: {
                                    type: 'localstorage',
                                    id  : 'timeStampStore'
                                    },
                            autoLoad: true
                            });
	  
	    /** 
         * Perfomr Ajax Request based on the url
         */
		 if(device.platform=='Win32NT')
		 {
		   cordova.exec(function(result){
			    console.log("FINAL RESULT (Process Cache):" +result);
                var responseText = result;
				//responseText = responseText.replace("https://mobileweb.sp.edu.sg/AlertWS/Images",tempURL);
				successCallbackProcess(responseText);
				
				/** 
                 * Create a DOM Html object
                 */
				var responseTextDOM = result;
				responseTextDOM = responseTextDOM.replace("<head>","<body>");
				responseTextDOM = responseTextDOM.replace("</head>","");
			    var cacheHtml = document.createElement("html");
                cacheHtml.innerHTML = responseTextDOM;
				
                /** 
                 * Process the Javascript Files
                 */
                var arrayJS = cacheHtml.getElementsByTagName('script');
                var arrayJSUrl = new Array();
                var arrayJSFilename = new Array();

				
				
                for (var i = 0; i < arrayJS.length; i++) {
                    try {
                        /** 
                         * Get the file path and file name
                         */
                        arrayJSUrl[i] = arrayJS[i].getAttribute("src");
                        arrayJSFilename[i] = arrayJS[i].getAttribute("src").replace(/^.*[\\\/]/, '').split('?')[0];
                        if ((arrayJSUrl[i].indexOf("http://") != -1) || (arrayJSUrl[i].indexOf("https://") != -1)) {
                        }
                        else {
                            arrayJSUrl[i] = sitePath + arrayJSUrl[i];
                        }
                    } 
                    catch (err) {
                        console.log("LOG: error");
                    }
                    
                    try {
                        if (arrayManifestCache.length > 0) {
                            for (var j = 0; j < arrayManifestCache.length; j++) {
                                if (arrayJSUrl[i] != null) {
                                    if (arrayJSUrl[i].indexOf(arrayManifestCache[j]) != -1) {
                                        /** 
                                         * Update the response using the lovcal directory path
                                         */
                                        responseText = responseText.replace(arrayJSUrl[i], SP.OfflineCache.getFileSystemRootPath() + "/"+arrayJSFilename[i]);
                                        try {
                                            /** 
                                             * Download the Javacript files
                                             */
             							cordova.exec(function(result){}, function(result){}, "OfflineCache", "downloadFile", [arrayJSUrl[i], {overwrite: true}, SP.OfflineCache.webSSOCookie]);                           
										} 
                                        catch (err) {
		                                    console.log("LOG: error download file");
                                        }
                                    }
                                }
                                
                            }
                            responseText = responseText.replace(arrayJSUrl[i], "");
                        }
                        else {
                            if (arrayJSUrl[i] != null) {
                                /** 
                                 * Update the response using the lovcal directory path
                                 */
                                responseText = responseText.replace(arrayJSUrl[i], SP.OfflineCache.getFileSystemRootPath()  +"/"+arrayJSFilename[i]);
                                try {
                                    /** 
                                     * Download the Javacript files
                                     */
 								cordova.exec(function(result){}, function(result){}, "OfflineCache", "downloadFile", [arrayJSUrl[i], {overwrite: true}, SP.OfflineCache.webSSOCookie]);                               } 
                                catch (err) {
                                    console.log("LOG: error download file");
                                }
                            }
                            
                        }
                    } 
                    catch (err) {
                    }
                    
                }
                // end of Process JS
                
                
                /** 
                 * Process the CSS files
                 */
                var arrayCSS = cacheHtml.getElementsByTagName("link");
                var arrayCSSUrl = new Array();
                var arrayCSSFilename = new Array();
                
                for (var i = 0; i < arrayCSS.length; i++) {
                    try {
                        /** 
                         * Get the file path and file name
                         */
                        arrayCSSUrl[i] = arrayCSS[i].getAttribute("href");
                        arrayCSSFilename[i] = arrayCSS[i].getAttribute("href").replace(/^.*[\\\/]/, '').split('?')[0];
                        if ((arrayCSSUrl[i].indexOf("http://") != -1) || (arrayCSSUrl[i].indexOf("https://") != -1)) {
                        }
                        else {
                            arrayCSSUrl[i] = sitePath + arrayCSSUrl[i];
                        }
                    } 
                    catch (err) {
                        console.log("LOG: error");
                    }
                    
                    try {
                        if (arrayManifestCache.length > 0) {
                            for (var j = 0; j < arrayManifestCache.length; j++) {
                                if (arrayCSSUrl[i] != null) {
                                    if (arrayCSSUrl[i].indexOf(arrayManifestCache[j]) != -1) {
                                        /** 
                                         * Update the response using the lovcal directory path
                                         */
                                        responseText = responseText.replace(arrayCSSUrl[i], SP.OfflineCache.getFileSystemRootPath() + "/"  + arrayCSSFilename[i]);
                                        try {
                                            /** 
                                             * Download the CSS files
                                             */
 											cordova.exec(function(result){}, function(result){}, "OfflineCache", "downloadFile", [arrayCSSUrl[i], {overwrite: true}, SP.OfflineCache.webSSOCookie]);                                       } 
                                        catch (err) {
                                            console.log("LOG: error download file");
                                        }
                                    }
                                }
                                
                            }
                            responseText = responseText.replace(arrayCSSUrl[i], "");
                        }
                        else {
                            if (arrayCSSUrl[i] != null) {
                                /** 
                                 * Update the response using the lovcal directory path
                                 */
                                responseText = responseText.replace(arrayCSSUrl[i], SP.OfflineCache.getFileSystemRootPath() + "/"+arrayCSSFilename[i]);
                                try {
                                    /** 
                                     * Download the CSS files
                                     */
								cordova.exec(function(result){}, function(result){}, "OfflineCache", "downloadFile", [arrayCSSUrl[i], {overwrite: true}, SP.OfflineCache.webSSOCookie]);                                } 
                                catch (err) {
                                    console.log("LOG: error download file");
                                }
                            }
                            
                        }
                    } 
                    catch (err) {
                    }
                    
                }
                // end of Process CSS
                
                
                
                /** 
                 * Process the Images
                 */
                var arrayImages = cacheHtml.getElementsByTagName("img");
                var arrayImagesUrl = new Array();
                var arrayImagesFilename = new Array();
				//alert("no. of images :"+arrayImages.length);
                
                for (var i = 0; i < arrayImages.length; i++) {
                    try {
                        /** 
                         * Get the file path and file name
                         */
                        arrayImagesUrl[i] = arrayImages[i].src;
                        arrayImagesFilename[i] = arrayImages[i].src.replace(/^.*[\\\/]/, '').split('?')[0];
                    } 
                    catch (err) {
                        console.log("LOG: error");
                    }
                    
                    try {
                        if (arrayManifestCache.length > 0) {
                            for (var j = 0; j < arrayManifestCache.length; j++) {
                                if (arrayImagesUrl[i] != null) {
                                    if (arrayImagesUrl[i].indexOf(arrayManifestCache[j]) != -1) {
                                        /** 
                                         * Update the response using the lovcal directory path
                                         */
                                        responseText = responseText.replace(arrayImagesUrl[i], SP.OfflineCache.getFileSystemRootPath() + "/"  +arrayImagesFilename[i]);
                                        responseText = responseText.replace(arrayImagesUrl[i], SP.OfflineCache.getFileSystemRootPath() + "/" +arrayImagesFilename[i]);
                                        try {
                                            /** 
                                             * Download the Image files
                                             */
									                         var fileURLvalue = arrayImagesUrl[i];
                         var filenameValue =  arrayImagesFilename[i];
                  
                         downloadFileCallback(filenameValue, fileURLvalue, function(result){
                                               var resultValues = result.split(";");
                                               var resultFilename = resultValues[0];
                                               var resultFileUrl = resultValues[1];
                                               var resultDateModified = resultValues[2];
                                               console.log("LOG: resultFileUrl: "+resultFileUrl);
                                               var recordFind;
                                               var storedDateModified;
                                               try
                                               {
                                               recordFind = TimestampOfflineStore.findRecord('filenameurl',resultFileUrl);
                                               storedDateModified = recordFind.get('date');
                                               }
                                               catch (err){
                                               }
                                               
                                               if(resultDateModified==storedDateModified)
                                               {
                                              // alert(resultFilename+" file NOT downloaded");
                                               console.log("LOG: "+resultFilename+" file NOT downloaded");
                                               }
                                               else
                                               {
                                              
                                              try
                                              {
                                              recordFind = TimestampOfflineStore.findRecord('filenameurl',resultFileUrl);
                                              TimestampOfflineStore.remove(recordFind);
                                              TimestampOfflineStore.sync();
                                              }
                                              catch (err){
                                              }
                                              
                                              TimestampOfflineStore.add({
                                                                         filenameurl: resultFileUrl,
                                                                         date: resultDateModified
                                                                         });
                                               
                                               TimestampOfflineStore.sync();
                                              //  alert(resultFilename+" file downloaded");
                                                console.log("LOG: "+resultFilename+" file downloaded");
                                              	cordova.exec(function(result){}, function(result){}, "OfflineCache", "downloadFile", [resultFileUrl, {overwrite: true}, SP.OfflineCache.webSSOCookie]);
											  // fileDownloadMgr.downloadFile(resultFileUrl, resultFilename);       
                                               }          
                                               });
                         
									//		PhoneGap.exec(function(result){}, function(result){}, "OfflineCache", "downloadFile", [arrayImagesUrl[i], {overwrite: true}]);
                                        } 
                                        catch (err) {
                                            console.log("LOG: error download file");
                                        }
                                    }
                                }
                                
                            }
                            responseText = responseText.replace(arrayImagesUrl[i], "");
                        }
                        else {
                            if (arrayImagesUrl[i] != null) {
                                /** 
                                 * Update the response using the lovcal directory path
                                 */
                                responseText = responseText.replace(arrayImagesUrl[i], SP.OfflineCache.getFileSystemRootPath() + "/"+arrayImagesFilename[i]);
                                responseText = responseText.replace(arrayImagesUrl[i], SP.OfflineCache.getFileSystemRootPath() + "/"+arrayImagesFilename[i]);
                                try {
                                    /** 
                                     * Download the Image files
                                     */
								            
                         var fileURLvalue = arrayImagesUrl[i];
                         var filenameValue =  arrayImagesFilename[i];
                  
                         downloadFileCallback(filenameValue, fileURLvalue, function(result){
                                               var resultValues = result.split(";");
                                               var resultFilename = resultValues[0];
                                               var resultFileUrl = resultValues[1];
                                               var resultDateModified = resultValues[2];
                                               console.log("LOG: resultFileUrl: "+resultFileUrl);
                                               var recordFind;
                                               var storedDateModified;
                                               try
                                               {
                                               recordFind = TimestampOfflineStore.findRecord('filenameurl',resultFileUrl);
                                               storedDateModified = recordFind.get('date');
                                               }
                                               catch (err){
                                               }
                                               
                                               if(resultDateModified==storedDateModified)
                                               {
                                             //  alert(resultFilename+" file NOT downloaded");
                                               console.log("LOG: "+resultFilename+" file NOT downloaded");
                                               }
                                               else
                                               {
                                              
                                              try
                                              {
                                              recordFind = TimestampOfflineStore.findRecord('filenameurl',resultFileUrl);
                                              TimestampOfflineStore.remove(recordFind);
                                              TimestampOfflineStore.sync();
                                              }
                                              catch (err){
                                              }
                                              
                                              TimestampOfflineStore.add({
                                                                         filenameurl: resultFileUrl,
                                                                         date: resultDateModified
                                                                         });
                                               
                                               TimestampOfflineStore.sync();
                                           //     alert(resultFilename+" file downloaded");
                                                console.log("LOG: "+resultFilename+" file downloaded");
                                              	cordova.exec(function(result){}, function(result){}, "OfflineCache", "downloadFile", [resultFileUrl, {overwrite: true}, SP.OfflineCache.webSSOCookie]);
											  // fileDownloadMgr.downloadFile(resultFileUrl, resultFilename);       
                                               }          
                                               });
                         

								
								} 
                                catch (err) {
								//	alert("error download");
                                    console.log("LOG: error download file");
                                }
                            }
                            
                        }
                    } 
                    catch (err) {
                    }
                    
                }
                // end of Process IMAGES
                
                
                
                SP.OfflineCache.createCache(fileNameCache, responseText);
                console.log("LOG: responseText: " + responseText);
									   
									}, function(error){}, "OfflineCache", "requestNative", [siteUrl,SP.WebSSOPlugin.getCookieStoredValue()]);
		 
		 
		 }
		 else if ((device.platform=='iOS')||(device.platform=='Android'))
		 {
		 
        Ext.Ajax.request({
            url: siteUrl,
            success: function(response, opts){
                var responseText = response.responseText;
                successCallbackProcess(responseText);
                
                /** 
                 * Create a DOM Html object
                 */
				var responseTextDOM = response.responseText;
				responseTextDOM = responseTextDOM.replace("<head>","<body>");
				responseTextDOM = responseTextDOM.replace("</head>","");
			    var cacheHtml = document.createElement("html");
                cacheHtml.innerHTML = responseTextDOM;
				
                /** 
                 * Process the Javascript Files
                 */
                var arrayJS = cacheHtml.getElementsByTagName('script');
                var arrayJSUrl = new Array();
                var arrayJSFilename = new Array();

                for (var i = 0; i < arrayJS.length; i++) {
                    try {
                        /** 
                         * Get the file path and file name
                         */
                        arrayJSUrl[i] = arrayJS[i].getAttribute("src");
                        arrayJSFilename[i] = arrayJS[i].getAttribute("src").replace(/^.*[\\\/]/, '').split('?')[0];
                        if ((arrayJSUrl[i].indexOf("http://") != -1) || (arrayJSUrl[i].indexOf("https://") != -1)) {
                        }
                        else {
                            arrayJSUrl[i] = sitePath + arrayJSUrl[i];
                        }
                    } 
                    catch (err) {
                        console.log("LOG: error");
                    }
                    
                    try {
                        if (arrayManifestCache.length > 0) {
                            for (var j = 0; j < arrayManifestCache.length; j++) {
                                if (arrayJSUrl[i] != null) {
                                    if (arrayJSUrl[i].indexOf(arrayManifestCache[j]) != -1) {
                                        /** 
                                         * Update the response using the lovcal directory path
                                         */
                                        responseText = responseText.replace(arrayJSUrl[i], SP.OfflineCache.getFileSystemRootPath() + "/"+arrayJSFilename[i]);
                                        try {
                                            /** 
                                             * Download the Javacript files
                                             */
             							cordova.exec(function(result){}, function(result){}, "OfflineCache", "downloadFile", [arrayJSUrl[i], {overwrite: true}, SP.OfflineCache.webSSOCookie]);                           
										} 
                                        catch (err) {
		                                    console.log("LOG: error download file");
                                        }
                                    }
                                }
                                
                            }
                            responseText = responseText.replace(arrayJSUrl[i], "");
                        }
                        else {
                            if (arrayJSUrl[i] != null) {
                                /** 
                                 * Update the response using the lovcal directory path
                                 */
                                responseText = responseText.replace(arrayJSUrl[i], SP.OfflineCache.getFileSystemRootPath()  +"/"+arrayJSFilename[i]);
                                try {
                                    /** 
                                     * Download the Javacript files
                                     */
 								cordova.exec(function(result){}, function(result){}, "OfflineCache", "downloadFile", [arrayJSUrl[i], {overwrite: true}, SP.OfflineCache.webSSOCookie]);                               } 
                                catch (err) {
                                    console.log("LOG: error download file");
                                }
                            }
                            
                        }
                    } 
                    catch (err) {
                    }
                    
                }
                // end of Process JS
                
                
                /** 
                 * Process the CSS files
                 */
                var arrayCSS = cacheHtml.getElementsByTagName("link");
                var arrayCSSUrl = new Array();
                var arrayCSSFilename = new Array();
                
                for (var i = 0; i < arrayCSS.length; i++) {
                    try {
                        /** 
                         * Get the file path and file name
                         */
                        arrayCSSUrl[i] = arrayCSS[i].getAttribute("href");
                        arrayCSSFilename[i] = arrayCSS[i].getAttribute("href").replace(/^.*[\\\/]/, '').split('?')[0];
                        if ((arrayCSSUrl[i].indexOf("http://") != -1) || (arrayCSSUrl[i].indexOf("https://") != -1)) {
                        }
                        else {
                            arrayCSSUrl[i] = sitePath + arrayCSSUrl[i];
                        }
                    } 
                    catch (err) {
                        console.log("LOG: error");
                    }
                    
                    try {
                        if (arrayManifestCache.length > 0) {
                            for (var j = 0; j < arrayManifestCache.length; j++) {
                                if (arrayCSSUrl[i] != null) {
                                    if (arrayCSSUrl[i].indexOf(arrayManifestCache[j]) != -1) {
                                        /** 
                                         * Update the response using the lovcal directory path
                                         */
                                        responseText = responseText.replace(arrayCSSUrl[i], SP.OfflineCache.getFileSystemRootPath() + "/"  + arrayCSSFilename[i]);
                                        try {
                                            /** 
                                             * Download the CSS files
                                             */
 											cordova.exec(function(result){}, function(result){}, "OfflineCache", "downloadFile", [arrayCSSUrl[i], {overwrite: true}, SP.OfflineCache.webSSOCookie]);                                       } 
                                        catch (err) {
                                            console.log("LOG: error download file");
                                        }
                                    }
                                }
                                
                            }
                            responseText = responseText.replace(arrayCSSUrl[i], "");
                        }
                        else {
                            if (arrayCSSUrl[i] != null) {
                                /** 
                                 * Update the response using the lovcal directory path
                                 */
                                responseText = responseText.replace(arrayCSSUrl[i], SP.OfflineCache.getFileSystemRootPath() + "/"+arrayCSSFilename[i]);
                                try {
                                    /** 
                                     * Download the CSS files
                                     */
								cordova.exec(function(result){}, function(result){}, "OfflineCache", "downloadFile", [arrayCSSUrl[i], {overwrite: true}, SP.OfflineCache.webSSOCookie]);                                } 
                                catch (err) {
                                    console.log("LOG: error download file");
                                }
                            }
                            
                        }
                    } 
                    catch (err) {
                    }
                    
                }
                // end of Process CSS
                
                
                
                /** 
                 * Process the Images
                 */
                var arrayImages = cacheHtml.getElementsByTagName("img");
                var arrayImagesUrl = new Array();
                var arrayImagesFilename = new Array();
				//alert("no. of images :"+arrayImages.length);
                
                for (var i = 0; i < arrayImages.length; i++) {
                    try {
                        /** 
                         * Get the file path and file name
                         */
                        arrayImagesUrl[i] = arrayImages[i].src;
                        arrayImagesFilename[i] = arrayImages[i].src.replace(/^.*[\\\/]/, '').split('?')[0];
                    } 
                    catch (err) {
                        console.log("LOG: error");
                    }
                    
                    try {
                        if (arrayManifestCache.length > 0) {
                            for (var j = 0; j < arrayManifestCache.length; j++) {
                                if (arrayImagesUrl[i] != null) {
                                    if (arrayImagesUrl[i].indexOf(arrayManifestCache[j]) != -1) {
                                        /** 
                                         * Update the response using the lovcal directory path
                                         */
                                        responseText = responseText.replace(arrayImagesUrl[i], SP.OfflineCache.getFileSystemRootPath() + "/"  +arrayImagesFilename[i]);
                                        responseText = responseText.replace(arrayImagesUrl[i], SP.OfflineCache.getFileSystemRootPath() + "/" +arrayImagesFilename[i]);
                                        try {
                                            /** 
                                             * Download the Image files
                                             */
									                         var fileURLvalue = arrayImagesUrl[i];
                         var filenameValue =  arrayImagesFilename[i];
                  
                         downloadFileCallback(filenameValue, fileURLvalue, function(result){
                                               var resultValues = result.split(";");
                                               var resultFilename = resultValues[0];
                                               var resultFileUrl = resultValues[1];
                                               var resultDateModified = resultValues[2];
                                               console.log("LOG: resultFileUrl: "+resultFileUrl);
                                               var recordFind;
                                               var storedDateModified;
                                               try
                                               {
                                               recordFind = TimestampOfflineStore.findRecord('filenameurl',resultFileUrl);
                                               storedDateModified = recordFind.get('date');
                                               }
                                               catch (err){
                                               }
                                               
                                        //      if(resultDateModified==storedDateModified)
                                         //      {
                                              // alert(resultFilename+" file NOT downloaded");
                                        //       console.log("LOG: "+resultFilename+" file NOT downloaded");
                                        //       }
                                        //       else
                                          //     {
                                              
                                              try
                                              {
                                              recordFind = TimestampOfflineStore.findRecord('filenameurl',resultFileUrl);
                                              TimestampOfflineStore.remove(recordFind);
                                              TimestampOfflineStore.sync();
                                              }
                                              catch (err){
                                              }
                                              
                                              TimestampOfflineStore.add({
                                                                         filenameurl: resultFileUrl,
                                                                         date: resultDateModified
                                                                         });
                                               
                                               TimestampOfflineStore.sync();
                                              //  alert(resultFilename+" file downloaded");
                                                console.log("LOG: "+resultFilename+" file downloaded");
                                            
                                                if(device.platform=='iOS')
                                                {
                                                	cordova.exec(function(result){}, function(result){}, "OfflineCache", "downloadFile", [resultFileUrl, resultFilename, SP.OfflineCache.webSSOCookie]);
                                                }
                                                else  if(device.platform=='Android')
                                                {
                                                    cordova.exec(function(result){}, function(result){}, "OfflineCache", "downloadFile", [resultFileUrl, {overwrite: true}, SP.OfflineCache.webSSOCookie]);
                                                 }
                                                
                                         	  // fileDownloadMgr.downloadFile(resultFileUrl, resultFilename);       
                                         //      }          
                                               });
                         
									//		PhoneGap.exec(function(result){}, function(result){}, "OfflineCache", "downloadFile", [arrayImagesUrl[i], {overwrite: true}]);
                                        } 
                                        catch (err) {
                                            console.log("LOG: error download file");
                                        }
                                    }
                                }
                                
                            }
                            responseText = responseText.replace(arrayImagesUrl[i], "");
                        }
                        else {
                            if (arrayImagesUrl[i] != null) {
                                /** 
                                 * Update the response using the lovcal directory path
                                 */
                                responseText = responseText.replace(arrayImagesUrl[i], SP.OfflineCache.getFileSystemRootPath() + "/"+arrayImagesFilename[i]);
                                responseText = responseText.replace(arrayImagesUrl[i], SP.OfflineCache.getFileSystemRootPath() + "/"+arrayImagesFilename[i]);
                                try {
                                    /** 
                                     * Download the Image files
                                     */
								            
                         var fileURLvalue = arrayImagesUrl[i];
                         var filenameValue =  arrayImagesFilename[i];
                  
                         downloadFileCallback(filenameValue, fileURLvalue, function(result){
                                               var resultValues = result.split(";");
                                               var resultFilename = resultValues[0];
                                               var resultFileUrl = resultValues[1];
                                               var resultDateModified = resultValues[2];
                                               console.log("LOG: resultFileUrl: "+resultFileUrl);
                                               var recordFind;
                                               var storedDateModified;
                                               try
                                               {
                                               recordFind = TimestampOfflineStore.findRecord('filenameurl',resultFileUrl);
                                               storedDateModified = recordFind.get('date');
                                               }
                                               catch (err){
                                               }
                                               
                                          //     if(resultDateModified==storedDateModified)
                                         //      {
                                             //  alert(resultFilename+" file NOT downloaded");
                                          //     console.log("LOG: "+resultFilename+" file NOT downloaded");
                                         // //     }
                                          //     else
                                           //    {
                                              
                                              try
                                              {
                                              recordFind = TimestampOfflineStore.findRecord('filenameurl',resultFileUrl);
                                              TimestampOfflineStore.remove(recordFind);
                                              TimestampOfflineStore.sync();
                                              }
                                              catch (err){
                                              }
                                              
                                              TimestampOfflineStore.add({
                                                                         filenameurl: resultFileUrl,
                                                                         date: resultDateModified
                                                                         });
                                               
                                               TimestampOfflineStore.sync();
                                           //     alert(resultFilename+" file downloaded");
                                                console.log("LOG: "+resultFilename+" file downloaded");
                                                if(device.platform=='iOS')
                                                {
                                                	cordova.exec(function(result){}, function(result){}, "OfflineCache", "downloadFile", [resultFileUrl, resultFilename, SP.OfflineCache.webSSOCookie]);
                                                }
                                                else  if(device.platform=='Android')
                                                {
                                                    cordova.exec(function(result){}, function(result){}, "OfflineCache", "downloadFile", [resultFileUrl, {overwrite: true}, SP.OfflineCache.webSSOCookie]);
                                                 }
                                                // fileDownloadMgr.downloadFile(resultFileUrl, resultFilename);       
                                             //  }          
                                               });
                         

								
								} 
                                catch (err) {
								//	alert("error download");
                                    console.log("LOG: error download file");
                                }
                            }
                            
                        }
                    } 
                    catch (err) {
                    }
                    
                }
                // end of Process IMAGES
                
                
                
                SP.OfflineCache.createCache(fileNameCache, responseText);
                console.log("LOG: responseText: " + responseText);
                
            },
            failure: function(response, opts){
              //  alert('fail');
            }
        });
       } 
    },
    
    /******************************************************
     * Success callback function
     *
     ******************************************************/
    success: function(parent){
        if (isLogEnabled) {
            console.log("LOG: Parent Name: " + parent.name);
        }
    },
    /* end of success */
    
    /******************************************************
     * Fail callback function
     *
     ******************************************************/
    fail: function(error){
        if (isLogEnabled) {
            console.log("LOG: Error code: " + error.code);
        }
    }
    /* end of fail */

};




     function downloadFileCallback(filenameValue, fileURLvalue,callBack)
                                              {
                                              Ext.Ajax.request({
                                                               url: fileURLvalue,
                                                               success: function(response, opts){
                                                               var dateModified = response.getResponseHeader('Last-Modified');
                                                               callBack(filenameValue+";"+fileURLvalue+";"+ dateModified);
                                                               },
                                                               failure: function(response, opts){
                                                           //    alert("failure");
                                                               }
                                                               });

                                              }
                                              
/*
setTimeout(function(success){
	
	
       SP.OfflineCache.readCache("fritzllanoracommobileSP_AboutPagehtml.cache", function(result){
           alert("result: "+result);
        });
	
	
}, 5000);
*/
 /* sample usage */ var tempURL = "http://fritzllanora.com/mobile";
/*  SP.OfflineCache.request("http://fritzllanora.com/sencha2/index.html", "http://fritzllanora.com/sencha2/offline_test2.manifest", function(result){
                Ext.getCmp("mainPanel").update(result);
            }, function(error){
            });
*/
            
